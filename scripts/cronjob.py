import logging
import re
from datetime import datetime

from app import db
from config import Config
from models import CronJob

logger = logging.getLogger(__name__)


def get_current_cronjobs():
    now = datetime.now()
    current_weekday = CronJob.allowed_weekdays[now.weekday()]
    current_time = now.time().replace(second=0, microsecond=0)

    cronjobs = CronJob.query \
        .filter_by(enabled=True, execute_time=current_time) \
        .filter(CronJob.execute_weekdays.like("%{}%".format(current_weekday))) \
        .order_by(CronJob.name.asc())

    return cronjobs


def get_scripts_for_cronjobs(query):
    all_scripts = ""

    for cronjob in query.all():
        all_scripts += "\n{}".format(cronjob.script)

    # Remove unnecessary newlines
    all_scripts = re.sub(r'(\n\r?)+', '\n', all_scripts)
    all_scripts = all_scripts.strip("\n")

    return all_scripts


def save_cronjob_from_request(cronjob, request):
    cronjob.from_request(request)

    validation_results = cronjob.validate()
    if validation_results is not None:
        logger.warning("Not a valid object: {}".format(validation_results))
        return validation_results

    db.session.add(cronjob)
    db.session.commit()

    return None


def save_cronjob(cronjob):
    validation_results = cronjob.validate()
    if validation_results is not None:
        logger.warning("Not a valid object: {}".format(validation_results))
        return validation_results

    db.session.add(cronjob)
    db.session.commit()

    return None


def delete_cronjob(cronjob: CronJob):
    db.session.delete(cronjob)
    db.session.commit()


def execute_current_cronjobs():
    cronjobs = get_current_cronjobs()

    if len(cronjobs.all()) == 0:
        logger.info("No current cronjobs found")
        return 0

    logger.info("{} current cronjobs found".format(len(cronjobs.all())))

    for cronjob in cronjobs:
        cronjob.execute()

    return len(cronjobs.all())
