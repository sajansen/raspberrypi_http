import json
import logging
from datetime import datetime, timedelta
from typing import List, Optional, Dict, Any

import requests
from sqlalchemy import func
from sqlalchemy.orm import defer, undefer, load_only

from app import db
from config import Config
from models import DownloadStat, DownloadStatSummary, TotalDownloadStats, TotalDownloadStatItem

logger = logging.getLogger(__name__)


def collect_download_stats() -> List[DownloadStat]:
    stats = get_all_stats()
    save_all_stats(stats)
    return stats


def save_all_stats(stats: List[DownloadStat]):
    logger.info("Saving download stats...")
    for stat in stats:
        save_download_stat(stat)


def get_all_stats() -> List[DownloadStat]:
    stats = list()

    for repository in Config.DOWNLOAD_STATS_GITHUB_REPOS:
        stats += get_stats_for_github(Config.DOWNLOAD_STATS_GITHUB_USER, repository)

    for repository in Config.DOWNLOAD_STATS_BITBUCKET_REPOS:
        stats += get_stats_for_bitbucket(Config.DOWNLOAD_STATS_BITBUCKET_USER, repository)

    logger.info("Retrieved {} stats".format(len(stats)))
    return stats


def get_stats_for_github(user: str, repository: str) -> List[DownloadStat]:
    logger.info("Getting download stats from GitHub...")
    url = Config.DOWNLOAD_STATS_GITHUB_URL.format(user=user, repository=repository)
    response = requests.get(url)

    # logger.debug(response.content)
    if not response.ok:
        raise response.raise_for_status()

    releases = response.json()

    stats = list()
    for release in releases:
        logger.debug(" - Processing release: {}".format(release["tag_name"]))
        for asset in release["assets"]:
            logger.debug("   - Processing asset: {}".format(asset["name"]))
            stats.append(DownloadStat(server="GitHub", user=user, repository=repository, name=asset["name"],
                                      download_count=asset["download_count"], tag=release["tag_name"]))

    return stats


def get_stats_for_bitbucket(user: str, repository: str) -> List[DownloadStat]:
    logger.info("Getting download stats from BitBucket...")
    url = Config.DOWNLOAD_STATS_BITBUCKET_URL.format(user=user, repository=repository)
    response = requests.get(url)

    # logger.debug(response.content)
    if not response.ok:
        raise response.raise_for_status()

    response_json = response.json()
    assets = response_json["values"]

    stats = list()
    for asset in assets:
        logger.debug("   - Processing asset: {}".format(asset["name"]))

        fix_upload_errors(asset)

        stats.append(DownloadStat(server="BitBucket", user=user, repository=repository, name=asset["name"],
                                  download_count=asset["downloads"]))

    return stats


# Fix some errors on git side, like a file that was re-uploaded lost all his download stats.
def fix_upload_errors(asset):
    if asset["name"] == "obs-scene-timer-client-v0.1.html":
        asset["downloads"] += 88
    elif asset["name"] == "obs-scene-timer-1.7.1.jar":
        asset["downloads"] += 604
    elif asset["name"] == "obs-scene-timer-1.7.1.exe":
        asset["downloads"] += 1378


def save_download_stat(stat: DownloadStat):
    db.session.add(stat)
    db.session.commit()


def get_all_download_stats() -> List[DownloadStat]:
    logger.info("Collecting all download stats")
    stats = DownloadStat.query \
        .order_by(DownloadStat.created_at.desc()) \
        .all()

    return stats


def delete_download_stat(stat: DownloadStat):
    logger.info("Deleting stat: {}".format(stat))
    db.session.delete(stat)
    db.session.commit()


def collect_all_names() -> Dict[str, List[str]]:
    logger.info("Collecting all names")

    repos = DownloadStat.query \
        .with_entities(DownloadStat.repository) \
        .distinct()

    repo_stats = {}
    for repo in repos:
        stats = DownloadStat.query \
            .filter_by(repository=repo.repository) \
            .with_entities(DownloadStat.name) \
            .distinct() \
            .all()
        repo_stats[repo.repository] = [stat.name for stat in stats]

    return repo_stats


def summary_for_name(repository: str, name: str) -> DownloadStatSummary:
    logger.info("Getting summary for name: {}".format(name))

    daily = _get_difference_by_date(repository, name,
                                    datetime.now() - timedelta(days=1),
                                    datetime.now() - timedelta(days=2))
    weekly = _get_difference_by_date(repository, name,
                                     datetime.now() - timedelta(weeks=1),
                                     datetime.now() - timedelta(weeks=2))
    monthly = _get_difference_by_date(repository, name,
                                      datetime.now() - timedelta(days=30),
                                      datetime.now() - timedelta(days=61))

    summary = DownloadStatSummary(
        repository=repository,
        name=name,
        daily=daily,
        weekly=weekly,
        monthly=monthly,
    )
    return summary


def _get_difference_by_date(repository, name, boundary_date: datetime, oldest_date: datetime):
    stats = DownloadStat.query \
        .filter_by(repository=repository, name=name) \
        .filter(DownloadStat.created_at >= oldest_date) \
        .order_by(DownloadStat.created_at.desc())

    current = stats.filter(DownloadStat.created_at >= boundary_date).first()
    previous = stats.filter(DownloadStat.created_at < boundary_date).first()

    if current is None:
        logger.info("No current DownloadStat found")
        return -1
    elif previous is None:
        logger.info("No previous DownloadStat found")
        return -1
    else:
        return current.download_count - previous.download_count


def all_from_last_month(repository: str) -> TotalDownloadStats:
    logger.info("Getting all from last month for repository: {}".format(repository))

    stats = DownloadStat.query \
        .filter_by(repository=repository) \
        .filter(DownloadStat.created_at >= datetime.now() - timedelta(days=30)) \
        .order_by(DownloadStat.created_at.desc())

    data = {}
    for stat in stats.all():
        date = stat.created_at - timedelta(minutes=stat.created_at.minute % 10,  # Round to 10 minutes
                                           seconds=stat.created_at.second,
                                           microseconds=stat.created_at.microsecond)
        data[date] = data.get(date, 0) + stat.download_count

    downloads = []
    for dat in data.items():
        downloads.append(TotalDownloadStatItem(
            download_count=dat[1],
            created_at=dat[0]))

    return TotalDownloadStats(downloads)


def all_from_last_month_for_asset(repository: str, name: str) -> List[DownloadStat]:
    logger.info("Getting all from last month for asset: {}".format(name))

    stats = DownloadStat.query \
        .filter_by(repository=repository, name=name) \
        .filter(DownloadStat.created_at >= datetime.now() - timedelta(days=30)) \
        .order_by(DownloadStat.created_at.desc())

    return stats.all()
