from flask import url_for, redirect

from app import app

from routes_api import *
from modules.analog_sensors.routes import *
from modules.wifi_scanner.routes import *
from modules.system_service.routes import *

logger = logging.getLogger(__name__)


@app.route('/')
def index():
    return app.send_static_file("index.html")


@app.route('/cronjobs')
def cronjobs_all():
    return redirect(url_for("index"))


@app.route('/cronjobs/now/execute')
def cronjobs_execute_current():
    executed = execute_current_cronjobs()
    return "{} cronjobs executed".format(executed)


@app.route('/cronjobs/execute/<int:id>')
def cronjobs_execute(id: int):
    cronjob = CronJob.query.get_or_404(id)
    cronjob.execute()
    return "Cronjob '{}' executed".format(cronjob.name)
