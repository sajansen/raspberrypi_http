#!/bin/bash

# Execute update procedure.
# On error: exit with message (although it's forbidden)

echo "Pulling from git..."
git pull || exit "Failed to pull from git"

. venv/bin/activate || exit "Failed to activate virtual environment"

echo ""
echo "Installing PIP requirements..."
pip install -r requirements || exit "Failed to install pip requirements"

echo ""
echo "Upgrading database..."
flask db upgrade || exit "Failed to upgrade database"

cd frontend
echo ""
echo "Installing frontend dependencies..."
yarn

echo ""
echo "Building frontend resources..."
date
yarn build
date
cd -

echo ""
echo "Restarting HTTP service"
sudo systemctl restart raspberry_http.service || exit "Failed to restart HTTP service"

echo ""
echo ""
echo ""
echo "DONE =)"
