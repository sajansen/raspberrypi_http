import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    HOST_IP = "0.0.0.0"
    HOST_PORT = 5000

    LOG_LEVEL = "DEBUG"

    # Database
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # MQTT
    MQTT_ENABLE = True
    MQTT_TOPIC = "light_controller"
    MQTT_HOST = "localhost"
    MQTT_PORT = 1883
    MQTT_USERNAME = "admin"
    MQTT_PASSWORD = "admin"

    DOWNLOAD_STATS_GITHUB_URL = "https://api.github.com/repos/{user}/{repository}/releases"
    DOWNLOAD_STATS_GITHUB_USER = "sampie777"
    DOWNLOAD_STATS_GITHUB_REPOS = ["obs-scene-que", "obs-scene-timer", "hymnbook2", ]
    DOWNLOAD_STATS_BITBUCKET_URL = "https://api.bitbucket.org/2.0/repositories/{user}/{repository}/downloads?pagelen=100"
    DOWNLOAD_STATS_BITBUCKET_USER = "sajansen"
    DOWNLOAD_STATS_BITBUCKET_REPOS = []

    # Analog sensors module
    analog_sensors_service_db = 'sqlite:///' + '/home/app.db'

    # WiFi scanner
    wifi_scanner_db = 'sqlite:///' + '/home/app.db'


local_config_ptah = os.path.join(basedir, "config_local.py")
if os.path.exists(local_config_ptah):
    from config_local import *
