from waitress import serve
import app
from config import Config
from mqtt_connection import mqtt_disconnect

serve(app.app, host=Config.HOST_IP, port=Config.HOST_PORT)

mqtt_disconnect()
