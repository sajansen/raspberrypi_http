#!/bin/bash

CRONJOB="* * * * * curl --insecure --silent http://127.0.0.1:5000/cronjobs/now/execute | /usr/bin/logger -t raspberrypi_http_cronjob"

SCRIPTPATH="$(
  cd "$(dirname "$0")"
  pwd -P
)"
PARENTPATH="${SCRIPTPATH}"

cat "${SCRIPTPATH}/raspberry_http.service.template" | sed "s#\/directorypath#${PARENTPATH}#g" >raspberry_http.service

# Deploy service file
echo "Deploying service"
sudo cp raspberry_http.service /etc/systemd/system/
sudo chmod 664 /etc/systemd/system/raspberry_http.service

# Clean up our garbage
echo "Cleaning up"
rm raspberry_http.service

# Run service file
echo "Enabling servie at boot"
sudo systemctl enable raspberry_http # Run at boot

# Setting up crontab
echo "Adding to crontab"
crontab -l |
  grep -v -F "raspberrypi_http_cronjob" |   # Remove any possible existing line for this service and
  {
    cat
    echo "${CRONJOB}"                 # append this service CRONJOb to the end of the crontab
  } |
  crontab -

echo "Done :)"
