from datetime import datetime

from app import app
from models import CronJob


@app.context_processor
def get_cronjob_allowed_weekdays():
    return dict(cronjob_allowed_weekdays=CronJob.allowed_weekdays)


@app.context_processor
def get_is_night_time():
    is_night_time = datetime.now().hour < 9 or datetime.now().hour >= 19
    return dict(is_night_time=is_night_time)
