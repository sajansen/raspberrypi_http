import React, {Component, ReactNode, Suspense} from 'react';
import {NotificationComponent} from "./modules/notifications/notifications";
import './App.sass';
import {Icon, Menu, Sidebar} from "semantic-ui-react";
import {getCookie, setCookie} from "./cookies";
import LoadingSplash from "./utils/LoadingSplash";
import {SemanticICONS} from "semantic-ui-react/dist/commonjs/generic";

export const CronJobs = React.lazy(() => import("./modules/cronjobs/CronJobs"));
export const DownloadStats = React.lazy(() => import("./modules/downloadStats/DownloadStats"));
export const Temperature = React.lazy(() => import("./modules/temperature/Temperature"));
export const LightLevel = React.lazy(() => import("./modules/lightLevel/LightLevel"));
export const WifiScanner = React.lazy(() => import("./modules/wifiScanner/WifiScanner"));
export const Axxess = React.lazy(() => import("./modules/axxess/Axxess"));
export const Security = React.lazy(() => import("./modules/security/SecurityScreen"));
export const ElectricityScreen = React.lazy(() => import("./modules/electricity/ElectricityScreen"));
export const WindowsScreen = React.lazy(() => import("./modules/windows/WindowsScreen"));

class View {
    title: string
    icon: SemanticICONS
    render: () => ReactNode

    constructor(title: string,
                icon: SemanticICONS,
                render: () => ReactNode) {
        this.title = title
        this.icon = icon
        this.render = render
    }
}

interface ComponentProps {
}

interface ComponentState {
    currentView: View,
    sidebarVisible: boolean,
}

export default class App extends Component<ComponentProps, ComponentState> {

    static readonly views = {
        TEMPERATURE: new View(
            "Temperature",
            "thermometer half",
            () => <Temperature/>),
        Electricity: new View(
            "Electricity",
            "lightning",
            () => <ElectricityScreen/>),
        WindowsScreen: new View(
            "Windows",
            "windows",
            () => <WindowsScreen/>),
        Axxess: new View(
            "Axxess",
            "wifi",
            () => <Axxess/>),
        Security: new View(
            "Security",
            "lock",
            () => <Security/>),
        LIGHT_LEVEL: new View(
            "Light level",
            "sun",
            () => <LightLevel/>),
        CRONJOBS: new View(
            "Cronjobs",
            "clock",
            () => <CronJobs/>),
        DOWNLOAD_STATS: new View(
            "Download Statistics",
            "chart bar",
            () => <DownloadStats/>),
        WifiScanner: new View(
            "Wifi",
            "fax",
            () => <WifiScanner/>),
    }

    constructor(props: ComponentProps) {
        super(props);

        this.state = {
            currentView: this.getViewFromCookie() || App.views.CRONJOBS,
            sidebarVisible: false,
        };

        this.setView = this.setView.bind(this);
        this.setSidebarVisible = this.setSidebarVisible.bind(this);
    }

    static isNightTime() {
        const now = new Date();
        return now.getHours() < 9 || now.getHours() >= 19
    }

    getViewFromCookie(): View | undefined {
        const lastView = getCookie("lastView");
        if (lastView === null) {
            return undefined;
        }
        return App.views[lastView as keyof typeof App.views];
    }

    setView(view: View) {
        const key = Object.keys(App.views).find(it => App.views[it as keyof typeof App.views] === view)
        if (!key) {
            console.error("View does not exists: ", view);
            return;
        }
        setCookie("lastView", key, 31 * 6);

        this.setState({
            currentView: view,
            sidebarVisible: false,
        });
    }

    setSidebarVisible(visible: boolean) {
        this.setState({
            sidebarVisible: visible,
        })
    }

    render() {
        const theme = App.isNightTime() ? "theme-dark" : "theme-light";
        document.body.classList.remove("theme-dark");
        document.body.classList.remove("theme-light");
        document.body.classList.add(theme);

        document.title = `${this.state.currentView.title} - Raspberry PI`

        return <div className={"App"}>
            <NotificationComponent/>

            <Sidebar.Pushable className={"sidebar-wrapper"}>
                <Sidebar
                    as={Menu}
                    animation='overlay'
                    icon='labeled'
                    inverted
                    onHide={() => this.setSidebarVisible(false)}
                    vertical
                    visible={this.state.sidebarVisible}
                    width='thin'
                >
                    <Menu.Item className={"title"}>
                        <Icon name='home'/>
                        <span>Raspberry PI</span>
                    </Menu.Item>
                    {Object.values(App.views).map(view =>
                        <Menu.Item as='a'
                                   key={view.title}
                                   onClick={() => this.setView(view)}>
                            <Icon name={view.icon}/>
                            <span>{view.title}</span>
                        </Menu.Item>
                    )}
                </Sidebar>

                <Sidebar.Pusher dimmed={this.state.sidebarVisible}>
                    <button className="ui black big right fixed sidebar-opener"
                            onClick={() => this.setSidebarVisible(!this.state.sidebarVisible)}>
                        <Icon name='sidebar'/>
                    </button>

                    <div className={'content'}>
                        <Suspense fallback={<LoadingSplash isLoading={true}
                                                           text={"Loading website..."}
                                                           title={"Loading content..."}/>}>
                            {this.state.currentView.render()}
                        </Suspense>
                    </div>
                </Sidebar.Pusher>
            </Sidebar.Pushable>
        </div>;
    }
}