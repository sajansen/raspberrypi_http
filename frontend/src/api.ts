import {apiHostUrl} from "./local_settings";

const apiBaseUrl = apiHostUrl + "/api/v1";
console.log("API base URL: " + apiBaseUrl);
export const apiHost = {
    axxess: window.location.protocol + "//" + window.location.hostname + ":7704",
    motionControlPanel: window.location.protocol + "//" + window.location.hostname + ":8081",
    motionLiveStream: window.location.protocol + "//" + window.location.hostname + ":8081",
}

const get = (url: string) => fetch(url, {
    method: "GET"
});

const post = (url: string, data = "") => fetch(url, {
    method: "POST",
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
});

// eslint-disable-next-line no-unused-vars
const remove = (url: string, data = "") => {
    // throw Error("Cannot perform DELETE request due to Cross Origin failures on server side")

    const headers = {
        'Content-Type': 'application/json'
    };

    return fetch(url, {
        method: "DELETE",
        body: JSON.stringify(data),
        headers: headers,
    });
}

// eslint-disable-next-line no-unused-vars
export const throwErrorsIfNotOk = (response: Response) => {
    if (response.status === 404) {
        throw Error(`Could not connect to server (${response.status})`);
    } else if (!response.ok) {
        throw Error(`Request failed: ${response.status} ${response.statusText}`);
    }
    return response;
}

export const api = {
    cronjobs: {
        list: () => get(apiBaseUrl + "/cronjobs"),
        add: (data: any) => post(apiBaseUrl + `/cronjobs/add`, data),
        save: (id: any, data: any) => post(apiBaseUrl + `/cronjobs/${id}/save`, data),
        execute: (id: any) => post(apiBaseUrl + `/cronjobs/${id}/execute`),
        delete: (id: any) => post(apiBaseUrl + `/cronjobs/${id}/delete`),

        now: {
            get: () => get(apiBaseUrl + "/cronjobs"),
            scripts: () => get(apiBaseUrl + "/cronjobs"),
            execute: () => post(apiBaseUrl + "/cronjobs"),
        }
    },
    mqtt: {
        status: () => get(apiBaseUrl + "/mqtt/status")
    },
    downloadStats: {
        repositories: () => get(apiBaseUrl + "/downloads/stats/repositories"),
        allPastMonth: (repository: any) => get(`${apiBaseUrl}/downloads/stats/repositories/${repository}/monthly`),

        assets: {
            summary: (repository: any, name: any) => get(`${apiBaseUrl}/downloads/stats/repositories/${repository}/asset/${name}/summary`),
            allPastMonth: (repository: any, name: any) => get(`${apiBaseUrl}/downloads/stats/repositories/${repository}/asset/${name}/monthly`),
        }
    },
    temperatures: {
        list: (hours = 24, desc = false) => get(`${apiBaseUrl}/sensors/temperatures?hours=${hours}&order=${desc ? "desc" : "asc"}`),
        latest: () => get(apiBaseUrl + "/sensors/temperatures/latest"),
        delete: (id: any) => remove(`${apiBaseUrl}/sensors/temperatures/${id}`),
    },
    lightLevels: {
        list: (hours = 24, desc = false) => get(`${apiBaseUrl}/sensors/light-levels?hours=${hours}&order=${desc ? "desc" : "asc"}`),
        latest: () => get(apiBaseUrl + "/sensors/light-levels/latest"),
        delete: (id: any) => remove(`${apiBaseUrl}/sensors/light-levels/${id}`),
    },
    wifiScanner: {
        list: () => get(`${apiBaseUrl}/wifi-scanner`),
        devices: () => get(apiBaseUrl + "/wifi-scanner/devices"),
        delete: (id: any) => remove(`${apiBaseUrl}/wifi-scanner/${id}`),
    },
    axxess: {
        list: (hours = 24) => get(`${apiHost.axxess}/list?hours=${hours}`),
        latest: () => get(apiHost.axxess + "/recent"),
    },
    electricity: {
        list: (hours = 24, desc = false) => get(`${apiBaseUrl}/sensors/electricity?hours=${hours}&order=${desc ? "desc" : "asc"}`),
        absolute: {
            list: (hours = 24, desc = false) => get(`${apiBaseUrl}/sensors/electricity/absolute?hours=${hours}&order=${desc ? "desc" : "asc"}`),
            create: (data: any) => post(`${apiBaseUrl}/sensors/electricity/absolute`, data),
            delete: (id: number) => remove(`${apiBaseUrl}/sensors/electricity/absolute/${id}`),
        }
    },
    security: {
        camera: {
            enable: () => get(`${apiBaseUrl}/system/motion/start`),
            disable: () => get(`${apiBaseUrl}/system/motion/stop`),
        }
    }
};