import React, {Component} from 'react';
import {api, throwErrorsIfNotOk} from "../../api";
import {addNotification, Notification} from "../notifications/notifications";
import {InternetDataLog} from "./models";
import CanvasJSReact from '../../assets/canvasjs.react';
import LoadingSplash from "../../utils/LoadingSplash";
import {bytesToSuffix, createDataSet, getExponentOf} from "./logic";

const CanvasJSChart = CanvasJSReact.CanvasJSChart;

interface ComponentProps {
}

interface ComponentState {
    readings: Array<InternetDataLog>
    isUpdating: boolean
    hours: number
}

export default class Chart extends Component<ComponentProps, ComponentState> {
    private _isMounted: boolean;

    private selectableHours = [
        {value: 24 * 7, text: "Last week",},
        {value: 24 * 7 * 2, text: "Last 2 weeks",},
        {value: Math.round(24 * 30.5), text: "Last month",},
        {value: Math.round(3 * 24 * 30.5), text: "Last 3 months",},
        {value: Math.round(6 * 24 * 30.5), text: "Last 6 months",},
        {value: Math.round(12 * 24 * 30.5), text: "Last year",},
    ]

    constructor(props: ComponentProps) {
        super(props);
        this._isMounted = false;

        this.state = {
            readings: [],
            isUpdating: false,
            hours: this.selectableHours[2].value
        };

        this.update = this.update.bind(this);
        this.onSelectChange = this.onSelectChange.bind(this);
        this.spreadDisruptedDataOverMultipleDatasets = this.spreadDisruptedDataOverMultipleDatasets.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        this.update();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    update() {
        if (!this._isMounted) return;
        this.setState({isUpdating: true});

        api.axxess.list(this.state.hours)
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then((data: Array<InternetDataLog>) => {
                if (!this._isMounted) return;

                data = data.sort((a, b) => a.timestamp.localeCompare(b.timestamp));
                this.setState({
                    readings: data,
                    isUpdating: false,
                });
            })
            .catch(error => {
                if (!this._isMounted) return;

                console.error(`Error fetching data`, error);
                addNotification(new Notification("Error fetching data",
                    error.message,
                    Notification.ERROR));

                this.setState({isUpdating: false,})
            });
    }

    private onSelectChange(e: React.FocusEvent<HTMLSelectElement>) {
        this.setState({
            hours: +e.target.value,
        }, this.update)
    }

    private spreadDisruptedDataOverMultipleDatasets() {
        const maxValue = Math.max.apply(null,
            this.state.readings.map(it => InternetDataLog.daytimeDataLeft(it)));
        const exponent = getExponentOf(maxValue);
        const suffix = bytesToSuffix(maxValue);

        const data = {
            daytime: createDataSet({
                toolTipContent: "Day time: {y} " + suffix + " at {x}",
                color: "#ff8c00",
                size: 4,
            }),
            nighttime: createDataSet({
                toolTipContent: "Night time: {y} " + suffix + " at {x}",
                color: "#5966a9"
            }),
            anytime: createDataSet({
                toolTipContent: "Any time: {y} " + suffix + " at {x}",
                color: "#cea901"
            }),
            rollover: createDataSet({
                toolTipContent: "Rollover: {y} " + suffix + " at {x}",
                color: "#da3edc"
            }),
            topup: createDataSet({
                toolTipContent: "Top-up: {y} " + suffix + " at {x}",
                color: "#f82b73"
            }),
            anytimeTotal: createDataSet({
                toolTipContent: "Any time total: {y} " + suffix + " at {x}",
                color: "#cea901",
                dash: true
            }),
        };

        this.state.readings
            .forEach(it => {
                const it_timestamp_date = new Date(it.timestamp);

                data.daytime.dataPoints.push({
                    x: it_timestamp_date,
                    y: InternetDataLog.daytimeDataLeft(it) / exponent,
                });
                data.nighttime.dataPoints.push({
                    x: it_timestamp_date,
                    y: InternetDataLog.nighttimeDataLeft(it) / exponent,
                });
                data.anytime.dataPoints.push({
                    x: it_timestamp_date,
                    y: (it.anytime_total - it.anytime_used) / exponent,
                });
                data.rollover.dataPoints.push({
                    x: it_timestamp_date,
                    y: (it.rollover_total - it.rollover_used) / exponent,
                });
                data.topup.dataPoints.push({
                    x: it_timestamp_date,
                    y: (it.topup_total - it.topup_used) / exponent,
                });
                data.anytimeTotal.dataPoints.push({
                    x: it_timestamp_date,
                    y: it.anytime_total / exponent,
                });
            });

        return Object.values(data);
    }

    render() {
        const maxValue = Math.max.apply(null,
            this.state.readings.map(it => InternetDataLog.daytimeDataLeft(it)));
        const data = this.spreadDisruptedDataOverMultipleDatasets();

        const options = {
            animationEnabled: true,
            exportEnabled: true,
            theme: "light2", // "light1", "dark1", "dark2",
            zoomEnabled: true,
            zoomType: "xy",
            backgroundColor: null,
            title: {
                text: `Last ${(this.state.hours / 24).toFixed(0)} days`,
                fontWeight: "normal",
                fontStyle: "italic",
                fontSize: 14,
            },
            axisY: {
                title: "Data left",
                includeZero: false,
                suffix: " " + bytesToSuffix(maxValue),
            },
            axisX: {
                title: "Time",
                prefix: "",
                valueFormatString: this.state.hours <= 24 * 7 ? "HH:mm, D MMMM" : "D MMMM",
            },
            data: data,
        }

        return <div className={"Chart"}>
            <LoadingSplash isLoading={this.state.isUpdating}/>

            {this.state.isUpdating ? undefined : <CanvasJSChart options={options}/>}

            <div className={"actions"}>
                <label>Change time span:</label>
                <select onChange={this.onSelectChange}>
                    {this.selectableHours.map(it =>
                        <option selected={this.state.hours === it.value}
                                value={it.value}
                                key={it.value}>
                            {it.text}
                        </option>)}
                </select>
            </div>
        </div>;
    }
}