import GraphData from "./GraphData";

export const getExponentOf = (value: number): number => {
    if (value === 0)
        return 1
    if (value < 1e3)
        return 1
    if (value < 1e6)
        return 1e3
    if (value < 1e9)
        return 1e6
    if (value < 1e12)
        return 1e9
    return 1e12
}

export const bytesToReadable = (value: number): number => {
    return value / getExponentOf(value)
}

export const bytesToSuffix = (value: number): string => {
    if (value === 0) {
        return "byte";
    }

    const exponent = getExponentOf(value);
    switch (exponent) {
        case 1:
            return "bytes";
        case 1e3:
            return "kB";
        case 1e6:
            return "Mb";
        case 1e9:
            return "GB";
        default:
            return "TB";
    }
}

export function createDataSet({
                                  toolTipContent,
                                  color = "#6d78ad",
                                  size = 1,
                                  dash = false
                              }: { toolTipContent?: string, color?: string, size?: number, dash?: boolean }): GraphData {
    return {
        type: "line",
        yValueFormatString: "#.#",
        toolTipContent: toolTipContent,
        dataPoints: [],
        markerSize: size,
        lineThickness: size,
        lineColor: color,
        markerColor: color,
        lineDashType: dash ? "dash" : "solid"
    };
}