import React, {Component} from 'react';
import './style.sass';
import Chart from "./Chart";
import CurrentStats from "./CurrentStats";

interface ComponentProps {
}

interface ComponentState {
}

export default class Axxess extends Component<ComponentProps, ComponentState> {

    constructor(props: ComponentProps) {
        super(props);

        this.state = {};
    }

    render() {
        return <div className={"Axxess"}>
            <h1>Axxess data</h1>
            <CurrentStats/>
            <Chart/>
        </div>;
    }
}