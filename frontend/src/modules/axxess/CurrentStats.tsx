import React, {Component} from 'react';
import {api, throwErrorsIfNotOk} from "../../api";
import {addNotification, Notification} from "../notifications/notifications";
import {Icon} from "semantic-ui-react";
import {InternetDataLog} from "./models";
import {bytesToReadable, bytesToSuffix} from "./logic";

interface ComponentProps {
}

interface ComponentState {
    reading?: InternetDataLog
    isUpdating: boolean
}

export default class CurrentStats extends Component<ComponentProps, ComponentState> {
    private _isMounted: boolean;

    constructor(props: ComponentProps) {
        super(props);
        this._isMounted = false;

        this.state = {
            isUpdating: false,
        };

        this.update = this.update.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        this.update();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    update() {
        if (!this._isMounted) return;
        this.setState({isUpdating: true});

        api.axxess.latest()
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then((data: InternetDataLog) => {
                if (!this._isMounted) return;

                this.setState({
                    reading: data,
                    isUpdating: false,
                });
            })
            .catch(error => {
                if (!this._isMounted) return;

                console.error(`Error fetching latest`, error);
                addNotification(new Notification("Error fetching latest",
                    error.message,
                    Notification.ERROR));

                this.setState({isUpdating: false,})
            });
    }

    render() {
        if (this.state.isUpdating) {
            return <div className={"CurrentStats"}>
                <Icon loading name="circle notched" title="Fetching latest reading..."/> Loading...
            </div>
        }

        if (this.state.reading === undefined || this.state.reading === null) {
            return <div className={"CurrentStats"}>
                <h4>No latest value...</h4>
            </div>;
        }

        const value = InternetDataLog.daytimeDataLeft(this.state.reading);
        const valueString = bytesToReadable(value)
            .toFixed(1)
            .split(".");
        return <div className={"CurrentStats"}>
            <h1>
                {valueString[0]}
                <small>
                    .{valueString[1]}
                </small>
                &nbsp;{bytesToSuffix(value)}
            </h1>
        </div>;
    }
}