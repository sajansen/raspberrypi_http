export class InternetDataLog {
    id: number = 0
    anytime_used: number = 0
    anytime_total: number = 0
    nighttime_used: number = 0
    nighttime_total: number = 0
    topup_used: number = 0
    topup_total: number = 0
    rollover_used: number = 0
    rollover_total: number = 0
    timestamp: string = (new Date()).toISOString()

    static daytimeDataLeft(item: InternetDataLog) {
        const total = item.anytime_total + item.topup_total + item.rollover_total;
        const used = item.anytime_used + item.topup_used + item.rollover_used;
        return total - used;
    }

    static nighttimeDataLeft(item: InternetDataLog) {
        return item.nighttime_total - item.nighttime_used;
    }
}