interface GraphDataPoint {
    x: Date
    y: number
}

export default interface GraphData {
    type?: string
    xValueFormatString?: string
    yValueFormatString?: string
    toolTipContent?: string
    dataPoints: Array<GraphDataPoint>
    markerSize?: number
    lineThickness?: number
    lineColor?: string
    markerColor?: string
    lineDashType?: "solid"| "shortDash"| "shortDot"| "shortDashDot"| "shortDashDotDot"| "dot"| "dash"| "dashDot"| "longDash"| "longDashDot"| "longDashDotDo"
    axisYType?: "primary" | "secondary"
    color?: string
}
