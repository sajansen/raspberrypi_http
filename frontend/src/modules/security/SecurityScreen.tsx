import React, {useState} from "react";
import './style.sass';
import {Icon} from "semantic-ui-react";
import {api, apiHost} from "../../api";
import {addNotification, Notification} from "../notifications/notifications";

type Props = {}

const SecurityScreen: React.FC<Props> = ({}) => {
    const [isLoading, setIsLoading] = useState(false);

    const enable = async () => {
        setIsLoading(true);
        try {
            await api.security.camera.enable();
            addNotification(new Notification("Camera", "Enabled", Notification.SUCCESS));
        } catch (error) {
            addNotification(new Notification("Camera", `Failed to enable. ${error}`, Notification.ERROR));
        }
        setIsLoading(false);
    }

    const disable = async () => {
        setIsLoading(true);
        try {
            await api.security.camera.disable();
            addNotification(new Notification("Camera", "Disabled", Notification.SUCCESS));
        } catch (error) {
            addNotification(new Notification("Camera", `Failed to disable. ${error}`, Notification.ERROR));
        }
        setIsLoading(false);
    }

    return <div className={"SecurityScreen"}>
        <h1>Security</h1>

        <h3>Live:</h3>
        <a href={`${apiHost.motionLiveStream}/0/stream`}>
            <img src={`${apiHost.motionLiveStream}/0/stream`}
                 alt={"Live camera stream"}
                 className={"stream"}/>
        </a>

        <p style={{marginTop: 20}}>
            <a href={`${apiHost.motionControlPanel}`}>
                <Icon name={"wrench"}/>
                Control panel
            </a>
        </p>

        <h3>Control</h3>
        <p className={"controls"}>
            <button onClick={enable}
                    disabled={isLoading}>
                Enable
            </button>
            <button onClick={disable}
                    disabled={isLoading}>
                Disable
            </button>
        </p>
    </div>
}

export default SecurityScreen;
