import React, {Component} from 'react';
import {api, throwErrorsIfNotOk} from "../../api";
import {addNotification, Notification} from "../notifications/notifications";
import {Icon} from "semantic-ui-react";
import {TempReading} from "./models";

interface ComponentProps {
}

interface ComponentState {
    reading?: TempReading
    isUpdating: boolean
}

export default class CurrentTemperature extends Component<ComponentProps, ComponentState> {
    private _isMounted: boolean;

    constructor(props: ComponentProps) {
        super(props);
        this._isMounted = false;

        this.state = {
            isUpdating: false,
        };

        this.update = this.update.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        this.update();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    update() {
        if (!this._isMounted) return;
        this.setState({isUpdating: true});

        api.temperatures.latest()
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then(data => {
                if (!this._isMounted) return;

                if (data.status === "error") {
                    throw new Error(data.data);
                }

                const reading: TempReading = data.data;

                this.setState({
                    reading: reading,
                    isUpdating: false,
                });
            })
            .catch(error => {
                if (!this._isMounted) return;

                console.error(`Error fetching latest temperature reading`, error);
                addNotification(new Notification("Error fetching latest temperature reading",
                    error.message,
                    Notification.ERROR));

                this.setState({isUpdating: false,})
            });
    }

    render() {
        if (this.state.isUpdating) {
            return <div className={"CurrentTemperature"}>
                <Icon loading name="circle notched" title="Fetching latest reading..."/> Loading...
            </div>
        }

        if (this.state.reading === undefined || this.state.reading === null) {
            return <div className={"CurrentTemperature"}>
                <h4>No latest reading...</h4>
            </div>;
        }

        const temperature = this.state.reading.temperature
            .toFixed(1)
            .split(".");
        return <div className={"CurrentTemperature"}>
            <h1>
                {temperature[0]}
                <small>
                    .{temperature[1]}
                </small>
                &nbsp;°C
            </h1>
        </div>;
    }
}