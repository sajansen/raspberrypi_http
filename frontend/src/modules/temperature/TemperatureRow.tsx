import React, {Component} from 'react';
import {TempReading} from "./models";
import {format} from "../../utils";
import {Button, Icon} from "semantic-ui-react";

interface ComponentProps {
    reading: TempReading
    onDelete: (reading: TempReading) => void
}

interface ComponentState {
}

export default class TemperatureRow extends Component<ComponentProps, ComponentState> {

    constructor(props: ComponentProps) {
        super(props);

        this.state = {};

    }

    render() {
        return <tr className="TemperatureRow">
            <td className={"date"}
                title={this.props.reading.created_at}>
                {format(this.props.reading.created_at, "%d %mmmm")}
            </td>
            <td className="time"
                title={this.props.reading.created_at}>
                <Icon name={"clock outline"}/>
                {format(this.props.reading.created_at, "%H:%MM")}
            </td>
            <td className={"temperature"}>
                <span className={"reading"}>{this.props.reading.temperature.toFixed(1)}</span>
                &nbsp;°C
            </td>
            <td className={"actions"}>
                <Button onClick={() => this.props.onDelete(this.props.reading)}
                        className={"delete"}>
                    <Icon name={"trash"} fitted={true}/>
                </Button>
            </td>
        </tr>;
    }
}