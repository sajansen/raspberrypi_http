export class TempReading {
    id: number = 0
    temperature: number = 0
    created_at: string = (new Date()).toISOString()
}