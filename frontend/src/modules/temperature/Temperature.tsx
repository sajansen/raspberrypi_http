import React, {Component} from 'react';
import './style.sass';
import TemperatureList from "./TemperatureList";
import CurrentTemperature from "./CurrentTemperature";
import TemperatureChart from "./TemperatureChart";

interface ComponentProps {
}

interface ComponentState {
}

export default class Temperature extends Component<ComponentProps, ComponentState> {

    constructor(props: ComponentProps) {
        super(props);

        this.state = {};
    }

    render() {
        return <div className={"Temperature"}>
            <h1>Temperature</h1>

            <CurrentTemperature/>

            <TemperatureChart/>

            <TemperatureList/>
        </div>;
    }
}