import React, {Component} from 'react';
import {api, throwErrorsIfNotOk} from "../../api";
import {addNotification, Notification} from "../notifications/notifications";
import {Button} from "semantic-ui-react";
import TemperatureRow from './TemperatureRow';
import {TempReading} from "./models";
import LoadingSplash from "../../utils/LoadingSplash";

interface ComponentProps {
}

interface ComponentState {
    readings: Array<TempReading>
    isUpdating: boolean
    hours: number
}

export default class TemperatureList extends Component<ComponentProps, ComponentState> {
    private _isMounted: boolean;

    constructor(props: ComponentProps) {
        super(props);
        this._isMounted = false;

        this.state = {
            readings: [],
            isUpdating: false,
            hours: 12
        };

        this.update = this.update.bind(this);
        this.loadMore = this.loadMore.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        this.update();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    update() {
        if (!this._isMounted) return;
        this.setState({isUpdating: true});

        api.temperatures.list(this.state.hours, true)
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then(data => {
                if (!this._isMounted) return;

                if (data.status === "error") {
                    throw new Error(data.data);
                }

                const readings: Array<TempReading> = data.data;

                this.setState({
                    readings: readings,
                    isUpdating: false,
                });
            })
            .catch(error => {
                if (!this._isMounted) return;

                console.error(`Error fetching temperature readings`, error);
                addNotification(new Notification("Error fetching temperature readings",
                    error.message,
                    Notification.ERROR));

                this.setState({isUpdating: false,})
            });
    }

    loadMore() {
        if (this.state.isUpdating) {
            return;
        }

        this.setState({
            hours: this.state.hours + 6
        }, this.update);
    }

    onDelete(reading: TempReading) {
        const result = window.confirm("Are you sure you want to delete this reading?")
        if (!result) {
            return;
        }

        if (!this._isMounted) return;

        api.temperatures.delete(reading.id)
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then(data => {
                if (!this._isMounted) return;

                if (data.status === "error") {
                    throw new Error(data.data);
                }

                addNotification(new Notification("Temperature reading deleted",
                    `Reading: ${reading.temperature.toFixed(1)} °C`,
                    Notification.SUCCESS));

                this.update();
            })
            .catch(error => {
                if (!this._isMounted) return;

                console.error(`Error deleting Temperature reading`, error);
                addNotification(new Notification("Error deleting temperature reading",
                    error.message,
                    Notification.ERROR));
            });
    }

    render() {

        return <div className={"TemperatureList"}>
            <LoadingSplash isLoading={this.state.isUpdating}/>

            {this.state.readings.length === 0 ? <h4>Much empty...</h4> : ""}
            <table>
                <tbody>
                {this.state.readings.map(it =>
                    <TemperatureRow key={it.id}
                                    reading={it}
                                    onDelete={this.onDelete}/>)}
                </tbody>
            </table>

            <div className={"actions"}>
                <Button onClick={this.loadMore}>
                    {this.state.isUpdating ? "Loading..." : "Load more"}
                </Button>
            </div>
        </div>;
    }
}