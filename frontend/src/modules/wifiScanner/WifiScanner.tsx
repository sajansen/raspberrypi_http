import React, {Component} from 'react';
import './style.sass';
import KnownDevices from "./KnownDevices";

interface ComponentProps {
}

interface ComponentState {
}

export default class WifiScanner extends Component<ComponentProps, ComponentState> {

    constructor(props: ComponentProps) {
        super(props);

        this.state = {};
    }

    render() {
        return <div className={"WifiScanner"}>
            <h1>Wifi Scanner</h1>

            <KnownDevices />
        </div>;
    }
}