import React, {Component} from 'react';
import {Device} from "./models";
import {format} from "../../utils";
import {Button, Icon} from "semantic-ui-react";

interface ComponentProps {
    device: Device
    onDelete: (device: Device) => void
}

interface ComponentState {
}

export default class KnownDeviceRow extends Component<ComponentProps, ComponentState> {

    constructor(props: ComponentProps) {
        super(props);

        this.state = {};

    }

    render() {
        return <tr className="KnownDeviceRow">
            <td className={"device_name"}>
                {this.props.device.device_name}
            </td>
            <td className={"ip"}>
                {this.props.device.ip}
            </td>
            <td className={"mac_address"}>
                {this.props.device.mac_address}
            </td>
            <td className={"connected_at"}
                title={this.props.device.connected_at == null ? "Not registered" : this.props.device.connected_at}>
                <Icon name={"calendar alternate outline"}/>
                {format(this.props.device.connected_at, "%d %mmmm %YYYY")}
                <Icon name={"clock outline"}/>
                {format(this.props.device.connected_at, "%H:%MM")}
            </td>
            <td className={"actions"}>
                <Button onClick={() => this.props.onDelete(this.props.device)}
                        className={"delete"}>
                    <Icon name={"trash"} fitted={true}/>
                </Button>
            </td>
        </tr>;
    }
}