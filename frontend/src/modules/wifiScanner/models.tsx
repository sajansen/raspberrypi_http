export enum Room {
    Unknown = "Unknown",
    Downstairs = "Downstairs",
    FirstFloor = "FirstFloor",
}

export class Device {
    id: number = 0
    device_name: string | null = null
    mac_address: string | null = null
    ip: string | null = null
    interface: string | null = null
    room: string | null = null
    connected_at: string | null = null
    disconnected_at: string | null = null
}