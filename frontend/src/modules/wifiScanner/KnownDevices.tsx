import React, {Component} from 'react';
import {api, throwErrorsIfNotOk} from "../../api";
import {addNotification, Notification} from "../notifications/notifications";
import KnownDeviceRow from './KnownDeviceRow';
import {Device} from "./models";
import LoadingSplash from "../../utils/LoadingSplash";

interface ComponentProps {
}

interface ComponentState {
    devices: Array<Device>
    isUpdating: boolean
}

export default class KnownDevices extends Component<ComponentProps, ComponentState> {
    private _isMounted: boolean;

    constructor(props: ComponentProps) {
        super(props);
        this._isMounted = false;

        this.state = {
            devices: [],
            isUpdating: false,
        };

        this.update = this.update.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        this.update();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    update() {
        if (!this._isMounted) return;
        this.setState({isUpdating: true});

        api.wifiScanner.devices()
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then(data => {
                if (!this._isMounted) return;

                if (data.status === "error") {
                    throw new Error(data.data);
                }

                const devices: Array<Device> = data.data;

                this.setState({
                    devices: devices,
                    isUpdating: false,
                });
            })
            .catch(error => {
                if (!this._isMounted) return;

                console.error(`Error fetching wifi devices`, error);
                addNotification(new Notification("Error fetching wifi devices",
                    error.message,
                    Notification.ERROR));

                this.setState({isUpdating: false,})
            });
    }

    onDelete(device: Device) {
        const result = window.confirm("Are you sure you want to delete this device?")
        if (!result) {
            return;
        }

        if (!this._isMounted) return;

        api.wifiScanner.delete(device.id)
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then(data => {
                if (!this._isMounted) return;

                if (data.status === "error") {
                    throw new Error(data.data);
                }

                addNotification(new Notification("Wifi device deleted",
                    `Device: ${device.device_name}`,
                    Notification.SUCCESS));

                this.update();
            })
            .catch(error => {
                if (!this._isMounted) return;

                console.error(`Error deleting wifi reading`, error);
                addNotification(new Notification("Error deleting wifi device",
                    error.message,
                    Notification.ERROR));
            });
    }

    render() {
        return <div className={"KnownDevices"}>
            <LoadingSplash isLoading={this.state.isUpdating}/>

            {this.state.devices.length === 0 ? <h4>Much empty...</h4> : ""}
            <table>
                <thead>
                <tr>
                    <th/>
                    <th/>
                    <th/>
                    <th>First connection</th>
                    <th/>
                </tr>
                </thead>
                <tbody>
                {this.state.devices.map(it =>
                    <KnownDeviceRow key={it.id}
                                    device={it}
                                    onDelete={this.onDelete}/>)}
                </tbody>
            </table>
        </div>;
    }
}