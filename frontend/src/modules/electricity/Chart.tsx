import React from "react";
import LoadingSplash from "../../utils/LoadingSplash";
import CanvasJSReact from "../../assets/canvasjs.react";
import {Electricity} from "./logic";

type Props = {
    values: Electricity.Type[]
    isUpdating: boolean
    hours: number
    setHours: (value: number) => void
}

const Chart: React.FC<Props> = ({values, isUpdating, hours, setHours}) => {
    const selectableHours = [
        {value: 2, text: "Last 2 hours",},
        {value: 6, text: "Last 6 hours",},
        {value: 12, text: "Last 12 hours",},
        {value: 24, text: "Last 24 hours",},
        {value: 36, text: "Last 36 hours",},
        {value: 24 * 2, text: "Last 2 days",},
        {value: 24 * 3, text: "Last 3 days",},
        {value: 24 * 7, text: "Last week",},
        {value: 24 * 7 * 2, text: "Last 2 weeks",},
        {value: Math.round(24 * 30.5), text: "Last month",},
        {value: Math.round(3 * 24 * 30.5), text: "Last 3 months",},
        {value: Math.round(6 * 24 * 30.5), text: "Last 6 months",},
        {value: Math.round(12 * 24 * 30.5), text: "Last year",},
    ];

    const options = {
        animationEnabled: true,
        exportEnabled: true,
        theme: "light2",
        zoomEnabled: true,
        zoomType: "xy",
        backgroundColor: null,
        title: {
            text: `Last ${hours} hours`,
            fontWeight: "normal",
            fontStyle: "italic",
            fontSize: 14,
        },
        axisX: {
            title: "Time",
            prefix: "",
            valueFormatString: hours <= 24 * 3 ? "HH:mm" : "D MMMM",
        },
        axisY: {
            title: "Cumulative usage",
            includeZero: false,
            suffix: " kWh",
            labelPlacement: "inside",
            labelFontSize: 10,
            titleFontSize: 14,
            margin: 0
        },
        axisY2: {
            title: "Instantaneous usage",
            includeZero: true,
            suffix: "",
            labelPlacement: "inside",
            labelFontSize: 8,
            titleFontSize: 12,
            margin: 0
        },
        data: Electricity.generateGraphData(values, hours),
    }

    return <div className={"Chart"}>
        <LoadingSplash isLoading={isUpdating}/>

        {isUpdating ? undefined : <CanvasJSReact.CanvasJSChart options={options}/>}

        <div className={"actions"}>
            <label>Change time span:</label>
            <select onChange={e => setHours(+e.target.value)}
                    defaultValue={hours}>
                {selectableHours.map(it =>
                    <option value={it.value}
                            key={it.value}>
                        {it.text}
                    </option>)}
            </select>
        </div>
    </div>;
}

export default Chart;
