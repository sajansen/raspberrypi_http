import React, {useState} from "react";
import { Electricity } from "../logic";
import {format} from "../../../utils";
import {Button, Icon} from "semantic-ui-react";
import {api, throwErrorsIfNotOk} from "../../../api";
import {addNotification, Notification} from "../../notifications/notifications";
import {useIsMounted} from "../../utils";

type Props = {
    reading: Electricity.AbsoluteType
}

const Row: React.FC<Props> = ({reading}) => {
    const isMounted = useIsMounted();
    const [isDeleted, setIsDeleted] = useState(false);

    const onDelete = () => {
        const result = window.confirm("Are you sure you want to delete this reading?")
        if (!result) {
            return;
        }

        if (!isMounted) return;

        api.electricity.absolute.delete(reading.id)
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then(data => {
                if (!isMounted) return;

                if (data.status === "error") {
                    throw new Error(data.data);
                }

                addNotification(new Notification("Electricity reading deleted",
                    `Reading: ${reading.reading.toFixed(1)}`,
                    Notification.SUCCESS));
                setIsDeleted(true)
            })
            .catch(error => {
                if (!isMounted) return;

                console.error(`Error deleting electricity reading`, error);
                addNotification(new Notification("Error deleting electricity reading",
                    error.message,
                    Notification.ERROR));
            });
    }

    return <div className={`reading ${isDeleted ? "deleted" : ""}`}>
        <div className={"timestamp"}>{format(reading.timestamp, "%dth %HH:%MM")}</div>
        <div className={"value"}>
            <span>{reading.reading.toFixed(1)}</span>
            <span> kWh</span>
        </div>
        <div className={"actions"}>
            <Button onClick={onDelete}
                    className={"delete"}>
                <Icon name={"trash"} fitted={true}/>
            </Button>
        </div>
    </div>
}

export default Row;
