import React from "react";
import {Electricity} from "../logic";
import {locale} from "../../../locale";
import {format} from "../../../utils";
import Row from "./Row";

type Props = {
    readings: Electricity.AbsoluteType[]
}

const AbsoluteReadings: React.FC<Props> = ({readings}) => {

    const orderData = () => {
        const result: { [key: number]: { [key: number]: Electricity.AbsoluteType[] } } = {};

        // Populate object with all past 12 months
        const now = new Date();
        for (let i = 0; i < 12; i++) {
            const then = new Date();
            then.setDate(1)
            then.setMonth(now.getMonth() - i);

            if (!Object.keys(result).includes(then.getFullYear().toString())) {
                result[then.getFullYear()] = {};
            }
            result[then.getFullYear()][then.getMonth()] = [];
        }

        readings.forEach(it => {
            const date = new Date(it.timestamp);
            if (result[date.getFullYear()][date.getMonth()] == null) {
                result[date.getFullYear()][date.getMonth()] = [];
            }
            result[date.getFullYear()][date.getMonth()].push(it)
        })

        return result;
    }

    const orderedData = orderData()

    return <div className={"AbsoluteReadings"}>
        {Object.keys(orderedData)
            .sort((a, b) => +b - +a)
            .map(year => <div key={year} className={"year"}>
                <div className={"title"}>{year}</div>
                {Object.keys(orderedData[+year])
                    .sort((a, b) => +b - +a)
                    .map(month => <div key={month} className={"month"}>
                        <div className={"title"}>{locale.en.constants.date.months[+month]}</div>
                        {orderedData[+year][+month]
                            .sort((a, b) => b.timestamp.localeCompare(a.timestamp))
                            .map(it => <Row key={it.timestamp} reading={it}/>)}
                    </div>)}
            </div>)}
    </div>;
}

export default AbsoluteReadings;
