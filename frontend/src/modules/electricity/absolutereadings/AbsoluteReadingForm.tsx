import React, {FormEvent, useState} from "react";
import {format} from "../../../utils";
import {api, throwErrorsIfNotOk} from "../../../api";
import {addNotification, Notification} from "../../notifications/notifications";
import {useIsMounted} from "../../utils";
import {Electricity} from "../logic";

type Props = {
    onSuccess?: () => void
    lastReading?: Electricity.AbsoluteType
}

const AbsoluteReadingForm: React.FC<Props> = ({onSuccess, lastReading}) => {
    const isMounted = useIsMounted()
    const [date, setDate] = useState(format(new Date(), "%YYYY-%mm-%dd"))
    const [time, setTime] = useState(format(new Date(), "%HH:%MM"))
    const [reading, setReading] = useState(lastReading?.reading)

    const onSubmit = (e: FormEvent) => {
        e.preventDefault()

        if (lastReading && reading == lastReading.reading) {
            if (!window.confirm("You are about to put in the same reading as last.\nAre you sure you want to continue?")) {
                return
            }
        }

        const obj = {
            reading: reading,
            timestamp: `${date} ${time}`
        }

        if (!isMounted) return;
        api.electricity.absolute.create(obj)
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then(data => {
                if (data.status === "error") {
                    throw new Error(data.data);
                }

                addNotification(new Notification("Reading added",
                    `${obj.reading} on ${obj.timestamp}`,
                    Notification.SUCCESS));
                onSuccess?.();
            })
            .catch(error => {
                console.error(`Error adding new reading`, error);
                addNotification(new Notification("Error adding new reading",
                    error.message,
                    Notification.ERROR));
            });
    }

    return <div className={"AbsoluteReadingForm"}>
        <form onSubmit={onSubmit}>
            <label>
                <span>Reading</span>
                <input type={"number"}
                       required
                       value={reading}
                       step={"0.1"}
                       onChange={e => setReading(+e.target.value)}/>
            </label>
            <label>
                <span>Date</span>
                <input type={"date"}
                       required
                       value={date}
                       onChange={e => setDate(e.target.value)}/>
            </label>
            <label>
                <span>Time</span>
                <input type={"time"}
                       required
                       value={time}
                       onChange={e => setTime(e.target.value)}/>
            </label>
            <button type={"submit"}>Submit</button>
        </form>
    </div>;
}

export default AbsoluteReadingForm;
