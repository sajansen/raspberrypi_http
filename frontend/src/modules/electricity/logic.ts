import GraphData from "../axxess/GraphData";

export namespace Electricity {
    export type Type = {
        id: number,
        pulses: number,
        created_at: string,
    }

    export type AbsoluteType = {
        id: number,
        reading: number,
        timestamp: string,
    }

    const conversionRate = 0.001

    export const totalUsage = (data: Type[]): number =>
        data.reduce((total, it) => total + it.pulses * conversionRate, 0);

    export const totalUsageCurrentMonth = (data: Type[]): number => {
        const monthStart = new Date()
        monthStart.setDate(1);
        monthStart.setHours(0);
        monthStart.setMonth(0);
        monthStart.setSeconds(0);

        return totalUsage(data.filter(it => (new Date(it.created_at).getTime() >= monthStart.getTime())))
    }

    export const averageUsageOverLastMinutes = (data: Type[], minutes: number): number => {
        const startDate = new Date((new Date()).getTime() - minutes * 60 * 1000);

        const filteredData = data.filter(it => (new Date(it.created_at).getTime() >= startDate.getTime()));
        return totalUsage(filteredData) * (60 / minutes);
    }

    export const generateGraphData = (rawData: Type[], hours: number): GraphData[] => {
        const data: GraphData[] = [];
        const cumulativeData: GraphData = {
            type: "area",
            xValueFormatString: hours <= 24 * 3 ? "HH:mm" : "D MMMM",
            yValueFormatString: "#.##",
            toolTipContent: "Total of {y} kWh at {x}",
            dataPoints: [],
            markerSize: 1,
            lineColor: "#6d78ad",
            axisYType: "primary",
            color: "rgba(140,148,185,0.32)",
        }

        rawData.forEach(it => {
            cumulativeData.dataPoints.push({
                x: new Date(it.created_at),
                y: cumulativeData.dataPoints.length === 0 ? 0 : cumulativeData.dataPoints[cumulativeData.dataPoints.length - 1].y + it.pulses * conversionRate,
            });
        })

        const createDataSet = (): GraphData => ({
            type: "stepLine",
            xValueFormatString: hours <= 24 * 3 ? "HH:mm" : "D MMMM",
            yValueFormatString: "#.###",
            toolTipContent: "{y} kWh at {x}",
            dataPoints: [],
            markerSize: 1,
            lineColor: "#ff8c00",
            axisYType: "secondary",
        })

        data.push(createDataSet())

        rawData.forEach(it => {
            let lastDataSet = data[data.length - 1];
            const lastDataPoint = lastDataSet.dataPoints[lastDataSet.dataPoints.length - 1];
            const it_created_at_date = new Date(it.created_at);

            if (lastDataPoint !== undefined && it_created_at_date.getTime() - lastDataPoint.x.getTime() > 35 * 60 * 1000) {
                data.push(createDataSet());
                lastDataSet = data[data.length - 1];
            }

            lastDataSet.dataPoints.push({
                x: it_created_at_date,
                y: it.pulses * conversionRate,
            });
        })

        data.push(cumulativeData)
        return data;
    }
}