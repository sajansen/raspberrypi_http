import React, {useEffect, useState} from "react";
import Chart from "./Chart";
import {Electricity} from "./logic";
import {api, throwErrorsIfNotOk} from "../../api";
import {addNotification, Notification} from "../notifications/notifications";
import {useIsMounted} from "../utils";
import {format} from "../../utils";
import './style.sass';
import AbsoluteReadingForm from "./absolutereadings/AbsoluteReadingForm";
import AbsoluteReadings from "./absolutereadings/AbsoluteReadings";

type Props = {}

const ElectricityScreen: React.FC<Props> = ({}) => {
    const isMounted = useIsMounted();
    const [data, setData] = useState<Electricity.Type[]>([]);
    const [absoluteReadings, setAbsoluteReadings] = useState<Electricity.AbsoluteType[]>([]);
    const [showNewReading, setShowNewReading] = useState(false);
    const [isUpdating, setIsUpdating] = useState(0);
    const [hours, setHours] = useState(36);

    useEffect(() => {
        update()
        updateAbsoluteReadings();
    }, []);

    useEffect(() => {
        update();
    }, [hours]);

    const update = async () => {
        if (!isMounted) return;
        setIsUpdating(isUpdating + 1);

        try {
            const response = await api.electricity.list(hours, true)
            throwErrorsIfNotOk(response);
            const json = await response.json();
            if (!isMounted) return;

            if (json.status === "error") {
                throw new Error(json.data);
            }

            const readings = (json.data as Electricity.Type[])
                .sort((a, b) => a.created_at.localeCompare(b.created_at))
            setData(readings);
        } catch (error) {
            if (!isMounted) return;

            console.error(`Error fetching electricity readings`, error);
            addNotification(new Notification("Error fetching electricity readings",
                error.message,
                Notification.ERROR));

        }
        setIsUpdating(isUpdating - 1);
    }

    const updateAbsoluteReadings = async () => {
        if (!isMounted) return;
        setIsUpdating(isUpdating + 1);

        try {
            const response = await api.electricity.absolute.list(24 * 30 * 12, true)
            throwErrorsIfNotOk(response);
            const json = await response.json();
            if (!isMounted) return;

            if (json.status === "error") {
                throw new Error(json.data);
            }

            const readings = (json.data as Electricity.AbsoluteType[])
                .sort((a, b) => a.timestamp.localeCompare(b.timestamp))
            setAbsoluteReadings(readings)
        } catch (error) {
            if (!isMounted) return;

            console.error(`Error fetching electricity absolute readings`, error);
            addNotification(new Notification("Error fetching electricity absolute readings",
                error.message,
                Notification.ERROR));

        }
        setIsUpdating(isUpdating - 1);
    }

    const toggleNewReading = () => {
        setShowNewReading(!showNewReading);
    }

    const averageUsageOverPeriod = data.length <= 1 ? 0 : Electricity.totalUsage(data) / (((new Date(data[data.length - 1].created_at)).getTime() - (new Date(data[0].created_at)).getTime()) / (60 * 60 * 1000))

    const onAbsoluteReadingAdded = () => {
        setShowNewReading(false);
        updateAbsoluteReadings();
    };

    return <div className={"ElectricityScreen"}>
        <h1>Electricity</h1>

        <p>
            Total used over the selected period: {Electricity.totalUsage(data).toFixed(1)} kWh
            ({(averageUsageOverPeriod * 1000).toFixed(0)} Wh per hour)<br/>
            Total used
            since {data.length === 0 ? 'now' : format(data[0].created_at, "%d %mmmm %YYYY")}: {Electricity.totalUsageCurrentMonth(data).toFixed(1)} kWh
        </p>

        <div>
            Current consumption:
            <table id={"usage-table"}>
                <thead>
                <tr>
                    <td>1 <small>min.</small></td>
                    <td>5 <small>min.</small></td>
                    <td>60 <small>min.</small></td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{(Electricity.averageUsageOverLastMinutes(data, 1) * 1000).toFixed(0)} <small>Wh</small></td>
                    <td><h3>{(Electricity.averageUsageOverLastMinutes(data, 5) * 1000).toFixed(0)} <small>Wh</small>
                    </h3></td>
                    <td>{(Electricity.averageUsageOverLastMinutes(data, 60) * 1000).toFixed(0)} <small>Wh</small></td>
                </tr>
                </tbody>
            </table>
        </div>

        <br/>

        <Chart values={data}
               isUpdating={isUpdating > 0}
               hours={hours}
               setHours={hours => setHours(hours)}/>

        <br/>

        <h3>Manually recorded readings</h3>

        <AbsoluteReadings readings={absoluteReadings}/>

        <br/>
        {!showNewReading
            ? <button onClick={toggleNewReading}>Record reading</button>
            : <AbsoluteReadingForm onSuccess={onAbsoluteReadingAdded}
                                   lastReading={absoluteReadings.length > 0 ? absoluteReadings[absoluteReadings.length - 1] : undefined}/>}
    </div>;
}

export default ElectricityScreen;
