export namespace Windows {
    export type Action = "open" | "hold" | "close";

    export const apply = (ip: string, action: Action): Promise<any> =>
        fetch(`http://${ip}/api/v1/window/${action}`)

    export type Status = {
        endstop_rod: number,
        endstop_rod_triggered: boolean,
        endstop_window: number,
        endstop_window_triggered: boolean,
        current_action: Action,
        uptime_ms: number,
    };

    const serverActionToLocalAction = (action: number): Action => {
        if (action == 1) return "open";
        if (action == -1) return "close";
        return "hold"
    }

    export const status = (ip: string): Promise<Status> => fetch(`http://${ip}/api/v1/window/status`)
        .then(r => r.json())
        .then(d => ({
            endstop_rod: d.endstop_rod,
            endstop_rod_triggered: d.endstop_rod_triggered == 1,
            endstop_window: d.endstop_window,
            endstop_window_triggered: d.endstop_window_triggered == 1,
            current_action: serverActionToLocalAction(d.current_action),
            uptime_ms: d.uptime_ms,
        }))

    export const statusToText = (status: Windows.Status) => {
        if (status.current_action == "open") return "Opening...";
        if (status.current_action == "close") return "Closing...";
        if (status.endstop_rod_triggered) return "Opened";
        if (status.endstop_window_triggered) return "Closed";
        return "Open";
    };
}