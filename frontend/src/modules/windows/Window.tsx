import React from "react";
import WindowControl from "./WindowControl";
import WindowStatus from "./WindowStatus";

type Props = {
    name: string
    ip: string
}

const Window: React.FC<Props> = ({name, ip}) => {

    return <div className={"Window"}>
        <h3>{name} <a href={`http://${ip}/api/v1/window/status`} className={"ip"} target={"_blank"}>{ip}</a></h3>

        <WindowStatus ip={ip}/>

        <div className={"controls"}>
            <WindowControl ip={ip} action={"close"}/>
            <WindowControl ip={ip} action={"hold"}/>
            <WindowControl ip={ip} action={"open"}/>
        </div>
    </div>
}

export default Window;
