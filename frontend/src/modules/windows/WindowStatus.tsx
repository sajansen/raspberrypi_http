import React, {useEffect, useState} from "react";
import {Windows} from "./logic";
import {useIsMounted} from "../utils";
import LoadingSplash from "../../utils/LoadingSplash";

type Props = { ip: string }

const WindowStatus: React.FC<Props> = ({ip}) => {
    const isMounted = useIsMounted();
    const [isLoading, setIsLoading] = useState(false);
    const [status, setStatus] = useState<Windows.Status | undefined>();

    const loadStatus = async (showLoadingIndicator = false) => {
        if (showLoadingIndicator) setIsLoading(true);

        try {
            const data = await Windows.status(ip);
            setStatus(data)
        } catch (error) {
            console.error("Failed to get window status", {
                ip: ip,
            })
            setStatus(undefined)
        }

        if (!isMounted) return;
        setIsLoading(false)
    };

    useEffect(() => {
        loadStatus(true);
        setInterval(() => loadStatus(false), 3000);
    }, []);

    return <div className={"WindowStatus"} onClick={() => loadStatus(true)}>
        <LoadingSplash isLoading={isLoading}/>
        {!status ? <p>No connection</p> : <p className={"status"}>{Windows.statusToText(status)}</p>}
    </div>;
}

export default WindowStatus;
