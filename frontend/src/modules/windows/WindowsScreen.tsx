import React from "react";
import './style.sass';
import Window from "./Window";

type Props = {}

const WindowsScreen: React.FC<Props> = ({}) => {
    const windows: { name: string, ip: string }[] = [
        {name: "Living room", ip: "192.168.0.90"},
    ]

    return <div className={"SecurityScreen"}>
        <h1>Windows</h1>

        {windows.map(it => <Window key={it.name} name={it.name} ip={it.ip}/>)}
    </div>
}

export default WindowsScreen;
