import React, {useState} from "react";
import {capitalize} from "../../utils";
import {useIsMounted} from "../utils";
import {Windows} from "./logic";

type Props = {
    ip: string
    action: Windows.Action
}

const WindowControl: React.FC<Props> = ({ip, action}) => {
    const isMounted = useIsMounted();
    const [isLoading, setIsLoading] = useState(false);

    const onClick = async () => {
        if (!isMounted || isLoading) return;

        setIsLoading(true);
        try {
            await Windows.apply(ip, action)
        } catch (error) {
            console.error("Failed to perform window action", {
                error: error,
                ip: ip,
                action: action,
            })
            alert(`Failed to ${action} window.\n\n${error}`)
        }

        if (!isMounted) return;
        setIsLoading(false)
    }

    return <button className={"WindowControl"}
                   disabled={isLoading}
                   onClick={onClick}>
        {capitalize(action)}
    </button>
}

export default WindowControl;
