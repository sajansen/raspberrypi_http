import React, {Component} from 'react';
import {api, throwErrorsIfNotOk} from "../../api";
import {addNotification, Notification} from "../notifications/notifications";
import {DownloadStat} from "./models";
import {Icon} from "semantic-ui-react";
import Graph from "../../utils/Graph";

interface ComponentProps {
    repository: string,
    name: string,
}

interface ComponentState {
    diffs: Array<Array<number>>,
    averagedDiffs: Array<Array<number>>,
    isUpdating: boolean,
}

export default class DownloadStatsGraph extends Component<ComponentProps, ComponentState> {
    private _isMounted: boolean;

    private width = 100;
    private height = 25;

    constructor(props: ComponentProps) {
        super(props);
        this._isMounted = false;

        this.state = {
            diffs: [],
            averagedDiffs: [],
            isUpdating: false,
        };

        this.update = this.update.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        this.update();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    update() {
        if (!this._isMounted) return;

        this.setState({isUpdating: true});

        api.downloadStats.assets.allPastMonth(this.props.repository, this.props.name)
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then(data => {
                if (!this._isMounted) return;

                if (data.status === "error") {
                    throw new Error(data.data);
                }

                const stats: Array<DownloadStat> = data.data;
                const diffs: Array<Array<number>> = this.createDiffsFromStats(stats);
                const averagedDiffs: Array<Array<number>> = this.createAveragesDiffsFromDiffs(diffs);

                this.setState({
                    diffs: diffs,
                    averagedDiffs: averagedDiffs,
                    isUpdating: false,
                });
            })
            .catch(error => {
                if (!this._isMounted) return;

                console.error(`Error fetching download stats for ${this.props.repository}:${this.props.name}`, error);
                addNotification(new Notification("Error fetching download stats for graph",
                    `Error fetching download stats for ${this.props.repository}:${this.props.name}.\n${error.message}`,
                    Notification.ERROR));

                this.setState({isUpdating: false,});
            });
    }

    private createDiffsFromStats(stats: Array<DownloadStat>): Array<Array<number>> {
        const diffs: Array<Array<number>> = [];
        stats.sort((a, b) => a.created_at?.localeCompare(b.created_at || "") || 0)
            .forEach((stat, i) => {
                if (i === 0) {
                    return;
                }

                const timestamp = (new Date(stat.created_at || "")).getTime()
                let diff = stat.download_count - stats[i - 1].download_count;
                diff = Math.max(0, diff);
                diffs.push([timestamp, diff]);
            });
        return diffs;
    }

    private createAveragesDiffsFromDiffs(diffs: Array<Array<number>>): Array<Array<number>> {
        if (diffs.length === 0) {
            return [];
        }

        // Average over these past samples
        const sampleIndices = [0, 1, 2, 3, 4, 5, 6];

        const result: Array<Array<number>> = [];
        diffs.forEach((diff, i) => {
            if (i < sampleIndices.length) {
                return;
            }

            let average = sampleIndices.map(it => diffs[i - it][1])
                .reduce(((previousValue, currentValue) => currentValue + previousValue)) / sampleIndices.length;

            result.push([diff[0], Math.round(average * 10) / 10]);
        });
        return result;
    }

    render() {
        return <div className={"DownloadStatsGraph"}>
            {this.state.isUpdating ?
                <Icon loading name="circle notched" title="Fetching data..."/> :
                <div>
                    <Graph height={this.height}
                           width={this.width}
                           title={"New downloads per sample"}
                           data={this.state.diffs}/>
                    <Graph height={this.height}
                           width={this.width}
                           title={"(Weekly) average of new downloads"}
                           data={this.state.averagedDiffs}/>
                </div>
            }
        </div>;
    }
}