export class Repository {
    name: string;
    assets: Array<string>;

    constructor(name: string, assets: Array<string>) {
        this.name = name
        this.assets = assets
    }
}

export class DownloadStat {
    id: number | null = null;
    server: string | undefined;
    user: string | undefined;
    repository: string | undefined;
    name: string | undefined;
    download_count: number = 0;
    tag: string | undefined;
    created_at: string | undefined;
}

export class TotalDownloadStatItem {
    download_count: number = 0;
    created_at: string | undefined;
}

export class TotalDownloadStats {
    downloads: Array<TotalDownloadStatItem> = [];
}