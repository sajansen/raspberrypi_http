import React, {Component} from 'react';
import {Icon} from "semantic-ui-react";

export enum DownloadStatSummaryValueType {
    UNKNOWN,
    LOADING,
    DAILY,
    WEEKLY,
    MONTHLY,
}

interface ComponentProps {
    value: number,
    type: DownloadStatSummaryValueType,
}

interface ComponentState {
}

export default class DownloadStatSummaryValue extends Component<ComponentProps, ComponentState> {

    constructor(props: ComponentProps) {
        super(props);

        this.state = {};
    }

    classNameForType(type: DownloadStatSummaryValueType) {
        switch (type) {
            case DownloadStatSummaryValueType.DAILY:
                return "daily";
            case DownloadStatSummaryValueType.WEEKLY:
                return "weekly";
            case DownloadStatSummaryValueType.MONTHLY:
                return "monthly";
            case DownloadStatSummaryValueType.LOADING:
                return "loading";
            default:
                return "unknown";
        }
    }

    render() {
        if (this.props.type === DownloadStatSummaryValueType.LOADING) {
            return <div className={this.classNameForType(this.props.type)}
                        title={this.classNameForType(this.props.type)}>
                <span><Icon loading name="circle notched" title="Fetching data..."/></span>
            </div>
        }

        if (this.props.value < 0) {
            return <div className={this.classNameForType(DownloadStatSummaryValueType.UNKNOWN)}
                        title={this.classNameForType(DownloadStatSummaryValueType.UNKNOWN)}>
                <span>-</span>
            </div>
        }

        return <div className={this.classNameForType(this.props.type)}
                    title={this.classNameForType(this.props.type)}>
            <span>{this.props.value}</span>
        </div>;
    }
}