import React, {Component} from 'react';
import DownloadStatAssetRow from "./DownloadStatRow";
import {Icon} from "semantic-ui-react";
import TotalDownloadsGraph from "./TotalDownloadsGraph";

interface ComponentProps {
    repository: string,
    assetNames: Array<string>,
}

interface ComponentState {
    showAssets: boolean,
}

export default class DownloadStatRepositoryRow extends Component<ComponentProps, ComponentState> {

    constructor(props: ComponentProps) {
        super(props);

        this.state = {
            showAssets: false,
        };

        this.toggleAssets = this.toggleAssets.bind(this);
    }

    toggleAssets() {
        this.setState({
            showAssets: !this.state.showAssets,
        })
    }

    render() {
        return <div className={"DownloadStatRepositoryRow"}>
            <div className={"repository"}
                 onClick={this.toggleAssets}
                 title={"repository"}>
                <Icon name="dropdown" rotated={this.state.showAssets ? undefined : "counterclockwise"}/>
                {this.props.repository}
            </div>

            {!this.state.showAssets ? "" : <div>
                <TotalDownloadsGraph repository={this.props.repository} />
                <div className={"DownloadStatAssetRow header"}>
                    <div className={"DownloadStatAssetNameHeader"}>Asset</div>
                    <div className={"DownloadStatSummaryHeader"}>Daily</div>
                    <div className={"DownloadStatSummaryHeader"}>Weekly</div>
                    <div className={"DownloadStatSummaryHeader"}>Monthly</div>
                </div>

                {this.props.assetNames.sort((a, b) => b.localeCompare(a)).map(it =>
                    <DownloadStatAssetRow key={it}
                                          repository={this.props.repository}
                                          name={it}/>)}
            </div>
            }
        </div>;
    }
}