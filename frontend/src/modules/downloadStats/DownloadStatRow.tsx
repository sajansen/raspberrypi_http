import React, {Component} from 'react';
import DownloadStatSummary from "./DownloadStatSummary";

interface ComponentProps {
    repository: string,
    name: string,
}

interface ComponentState {
}

export default class DownloadStatRow extends Component<ComponentProps, ComponentState> {

    constructor(props: ComponentProps) {
        super(props);

        this.state = {};
    }

    render() {
        return <div className={"DownloadStatAssetRow"}>
            <div className={"name"} title={"name"}>{this.props.name}</div>
            <DownloadStatSummary repository={this.props.repository} name={this.props.name} />
        </div>;
    }
}