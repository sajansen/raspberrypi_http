import React, {Component} from 'react';
import {api, throwErrorsIfNotOk} from "../../api";
import {addNotification, Notification} from "../notifications/notifications";
import DownloadStatSummaryValue, {DownloadStatSummaryValueType} from "./DownloadStatSummaryValue";
import DownloadStatsGraph from "./DownloadStatsGraph";

interface ComponentProps {
    repository: string,
    name: string,
}

interface ComponentState {
    daily: number,
    weekly: number,
    monthly: number,
    isUpdating: boolean,
}

export default class DownloadStatSummary extends Component<ComponentProps, ComponentState> {
    private _isMounted: boolean;

    constructor(props: ComponentProps) {
        super(props);
        this._isMounted = false;

        this.state = {
            daily: 0,
            weekly: 0,
            monthly: 0,
            isUpdating: false,
        };

        this.update = this.update.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        this.update();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    update() {
        if (!this._isMounted) return;

        this.setState({isUpdating: true});

        api.downloadStats.assets.summary(this.props.repository, this.props.name)
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then(data => {
                if (!this._isMounted) return;

                if (data.status === "error") {
                    throw new Error(data.data);
                }

                this.setState({
                    daily: data.data.daily,
                    weekly: data.data.weekly,
                    monthly: data.data.monthly,
                    isUpdating: false,
                })
            })
            .catch(error => {
                if (!this._isMounted) return;

                console.error(`Error fetching download stat summary for ${this.props.repository}:${this.props.name}`, error);
                addNotification(new Notification("Error fetching download stat summary",
                    `Error fetching download stat summary for ${this.props.repository}:${this.props.name}.\n${error.message}`,
                    Notification.ERROR));
            });
    }

    render() {
        if (this.state.isUpdating) {
            return <div className={"DownloadStatSummary"}>
                <DownloadStatSummaryValue value={-1} type={DownloadStatSummaryValueType.LOADING}/>
                <DownloadStatSummaryValue value={-1} type={DownloadStatSummaryValueType.LOADING}/>
                <DownloadStatSummaryValue value={-1} type={DownloadStatSummaryValueType.LOADING}/>
            </div>
        }

        return <div className={"DownloadStatSummary"}>
            <DownloadStatSummaryValue value={this.state.daily} type={DownloadStatSummaryValueType.DAILY}/>
            <DownloadStatSummaryValue value={this.state.weekly} type={DownloadStatSummaryValueType.WEEKLY}/>
            <DownloadStatSummaryValue value={this.state.monthly} type={DownloadStatSummaryValueType.MONTHLY}/>
            <DownloadStatsGraph name={this.props.name} repository={this.props.repository} />
        </div>;
    }
}