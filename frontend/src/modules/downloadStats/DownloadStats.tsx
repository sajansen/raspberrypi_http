import React, {Component} from 'react';
import {Repository} from "./models";
import DownloadStatRepositoryRow from "./DownloadStatRepositoryRow";
import {api, throwErrorsIfNotOk} from "../../api";
import {addNotification, Notification} from "../notifications/notifications";
import './downloadstats.sass';
import {Icon} from "semantic-ui-react";

interface ComponentProps {
}

interface ComponentState {
    repositories: Array<Repository>,
    isUpdating: boolean,
}

export default class DownloadStats extends Component<ComponentProps, ComponentState> {
    private _isMounted: boolean;

    constructor(props: ComponentProps) {
        super(props);
        this._isMounted = false;

        this.state = {
            repositories: [],
            isUpdating: false,
        };

        this.update = this.update.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        this.update();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    update() {
        if (!this._isMounted) return;
        this.setState({isUpdating: true});

        api.downloadStats.repositories()
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then(data => {
                if (!this._isMounted) return;

                if (data.status === "error") {
                    throw new Error(data.data);
                }

                const repositories: Array<Repository> = Object.entries(data.data)
                    .map(([key, value]) =>
                        new Repository(key, value as Array<string>));

                this.setState({
                    repositories: repositories,
                    isUpdating: false,
                });
            })
            .catch(error => {
                if (!this._isMounted) return;

                console.error(`Error fetching download stat repositories`, error);
                addNotification(new Notification("Error fetching download stat repositories",
                    error.message,
                    Notification.ERROR));

                this.setState({isUpdating: false,})
            });
    }

    render() {
        if (this.state.isUpdating) {
            return <div className={"DownloadStats"}>
                <Icon loading name="circle notched" title="Fetching repositories..."/> Loading...
            </div>
        }

        return <div className={"DownloadStats"}>
            <h1>Download statistics</h1>
            {this.state.repositories.length === 0 ? <h4>Much empty...</h4> : ""}
            {this.state.repositories.map(repo =>
                <DownloadStatRepositoryRow key={repo.name}
                                           repository={repo.name}
                                           assetNames={repo.assets}/>)}
        </div>;
    }
}