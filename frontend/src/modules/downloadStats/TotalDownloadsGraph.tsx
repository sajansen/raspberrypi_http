import React, {Component} from 'react';
import Graph from "../../utils/Graph";
import {api, throwErrorsIfNotOk} from "../../api";
import {TotalDownloadStatItem, TotalDownloadStats} from "./models";
import {addNotification, Notification} from "../notifications/notifications";
import {Icon} from "semantic-ui-react";

interface ComponentProps {
    repository: string,
}

interface ComponentState {
    data: Array<Array<number>>,
    averages: Array<Array<number>>,
    isUpdating: boolean,
}

export default class TotalDownloadsGraph extends Component<ComponentProps, ComponentState> {
    private _isMounted: boolean;

    constructor(props: ComponentProps) {
        super(props);
        this._isMounted = false;

        this.state = {
            data: [],
            averages: [],
            isUpdating: false,
        };

        this.update = this.update.bind(this);
        this.processData = this.processData.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        this.update();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    update() {
        if (!this._isMounted) return;

        this.setState({isUpdating: true});

        api.downloadStats.allPastMonth(this.props.repository)
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then(data => {
                if (!this._isMounted) return;

                if (data.status === "error") {
                    throw new Error(data.data);
                }

                let totalDownloadStats: TotalDownloadStats = data.data;
                const stats: Array<Array<number>> = this.processData(totalDownloadStats.downloads);
                const averages: Array<Array<number>> = this.createAveragesDiffsFromData(stats);

                this.setState({
                    data: stats,
                    averages: averages,
                    isUpdating: false,
                });
            })
            .catch(error => {
                if (!this._isMounted) return;

                console.error(`Error fetching download stats for ${this.props.repository}`, error);
                addNotification(new Notification("Error fetching download stats for graph",
                    `Error fetching download stats for ${this.props.repository}.\n${error.message}`,
                    Notification.ERROR));

                this.setState({isUpdating: false,});
            });
    }

    private processData(data: Array<TotalDownloadStatItem>): Array<Array<number>> {
        const result: Array<Array<number>> = [];
        data.sort((a, b) => a.created_at?.localeCompare(b.created_at || "") || 0)
            .forEach((stat, i) => {
                if (i === 0) {
                    return;
                }

                const timestamp = (new Date(stat.created_at || "")).getTime()
                result.push([timestamp, stat.download_count]);
            });
        return result;
    }

    private createAveragesDiffsFromData(data: Array<Array<number>>): Array<Array<number>> {
        if (data.length === 0) {
            return [];
        }

        // Average over these past samples
        const sampleIndices = [0, 1, 2, 3, 4, 5, 6];

        const result: Array<Array<number>> = [];
        const diffs = data.map((stat, i) => {
            if (i === 0) {
                return [0, 0];
            }

            let diff = data[i][1] - data[i - 1][1];
            return [stat[0], Math.max(0, diff)];
        });

        diffs.forEach((diff, i) => {
            if (i < sampleIndices.length) {
                return;
            }

            let average = sampleIndices.map(it => diffs[i - it][1])
                .reduce(((previousValue, currentValue) => currentValue + previousValue)) / sampleIndices.length;

            result.push([diff[0], Math.round(average * 10) / 10]);
        });
        return result;
    }

    render() {
        return <div className={"TotalDownloadsGraph"}>
            {this.state.isUpdating ?
                <Icon loading name="circle notched" title="Fetching data..."/> :
                <div>
                    {/*<Graph data={this.state.data}*/}
                    {/*       height={50}*/}
                    {/*       width={300}*/}
                    {/*       title={"Total downloads per sample"}*/}
                    {/*       fitYAxis={true}/>*/}
                    <Graph data={this.state.averages}
                           height={50}
                           width={300}
                           title={"Moving (weekly) average of new downloads"}
                           fitYAxis={false}/>
                </div>
            }
        </div>;
    }
}