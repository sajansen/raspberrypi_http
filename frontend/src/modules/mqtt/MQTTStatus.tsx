import React, {Component} from 'react';
import "./mqtt.sass"
import {api, throwErrorsIfNotOk} from "../../api";
import {addNotification, Notification} from "../notifications/notifications";
import {Icon} from "semantic-ui-react";

interface ComponentProps {
}

interface ComponentState {
    username: string,
    host: string,
    isConnected: boolean,
    isUpdating: boolean,
}

export default class MQTTStatus extends Component<ComponentProps, ComponentState> {
    private _isMounted: boolean;
    private _updateTimer: number | undefined;
    private updateInterval = 3000;

    constructor(props: ComponentProps) {
        super(props);
        this._isMounted = false;

        this.state = {
            username: "",
            host: "",
            isConnected: false,
            isUpdating: false,
        };

        this.update = this.update.bind(this);
        this.cancelInterval = this.cancelInterval.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        this.update();
        this._updateTimer = window.setInterval(this.update, this.updateInterval)
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    cancelInterval() {
        if (this._updateTimer) {
            window.clearInterval(this._updateTimer);
        }
    }

    update() {
        if (!this._isMounted) {
            this.cancelInterval();
            return;
        }

        this.setState({isUpdating: true});

        api.mqtt.status()
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then(data => {
                if (!this._isMounted) return;

                if (data.status === "error") {
                    throw new Error(data.data);
                }

                this.setState({
                    username: data.data.username,
                    host: data.data.host,
                    isConnected: data.data.isConnected,
                    isUpdating: false,
                })
            })
            .catch(error => {
                if (!this._isMounted) return;

                console.error("Error getting MQTT status", error);
                addNotification(new Notification("Error getting MQTT status",
                    error.message,
                    Notification.ERROR));

                this.setState({isUpdating: false,})
            });
    }

    render() {
        return <div className={"MQTTStatus"}>
            <a href={`http://${window.location.hostname}:15672`}
               title="Control panel"
               target="_blank">MQTT</a>:&nbsp;
            {this.state.isUpdating ?
                <span><Icon loading name="circle notched" title="Getting status..."/></span> :
                <span>{this.state.username}@{this.state.host} - {this.state.isConnected ? "Connected" : "Disconnected"}</span>}
        </div>;
    }
}