import React, {Component} from 'react';
import './style.sass';
import LightLevelList from "./LightLevelList";
import CurrentLightLevel from "./CurrentLightLevel";
import LightLevelChart from "./LightLevelChart";

interface ComponentProps {
}

interface ComponentState {
}

export default class LightLevel extends Component<ComponentProps, ComponentState> {

    constructor(props: ComponentProps) {
        super(props);

        this.state = {};
    }

    render() {
        return <div className={"LightLevel"}>
            <h1>Light level</h1>

            <CurrentLightLevel/>

            <LightLevelChart/>

            <LightLevelList/>
        </div>;
    }
}