import React, {Component} from 'react';
import {LightReading} from "./models";
import {format} from "../../utils";
import {Button, Icon} from "semantic-ui-react";

interface ComponentProps {
    reading: LightReading
    onDelete: (reading: LightReading) => void
}

interface ComponentState {
}

export default class LightLevelRow extends Component<ComponentProps, ComponentState> {

    constructor(props: ComponentProps) {
        super(props);

        this.state = {};

    }

    render() {
        return <tr className="LightLevelRow">
            <td className={"date"}
                title={this.props.reading.created_at}>
                {format(this.props.reading.created_at, "%d %mmmm")}
            </td>
            <td className="time"
                title={this.props.reading.created_at}>
                <Icon name={"clock outline"} />
                {format(this.props.reading.created_at, "%H:%MM")}
            </td>
            <td className={"lightLevel"}>
                <span className={"reading"}>{Math.round(this.props.reading.resistance)}</span>
                &nbsp;Ω
            </td>
            <td className={"lux"}>
                {this.props.reading.resistance <= 0 ? null : <>
                    {Math.round(LightReading.toLux(this.props.reading))}&nbsp;lux
                </>}
            </td>
            <td className={"actions"}>
                <Button onClick={() => this.props.onDelete(this.props.reading)}
                        className={"delete"}>
                    <Icon name={"trash"} fitted={true}/>
                </Button>
            </td>
        </tr>;
    }
}