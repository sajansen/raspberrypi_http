export class LightReading {
    id: number = 0
    raw_value: number = 0
    resistance: number = 0
    created_at: string = (new Date()).toISOString()

    static toLux(reading: LightReading) {
        // Source: https://www.allaboutcircuits.com/projects/design-a-luxmeter-using-a-light-dependent-resistor/
        return (1.25 * 10 ** 7) * reading.resistance ** -1.4059;
    }
}