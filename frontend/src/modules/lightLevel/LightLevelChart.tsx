import React, {Component} from 'react';
import {api, throwErrorsIfNotOk} from "../../api";
import {addNotification, Notification} from "../notifications/notifications";
import {LightReading} from "./models";
import CanvasJSReact from '../../assets/canvasjs.react';
import LoadingSplash from "../../utils/LoadingSplash";

const CanvasJSChart = CanvasJSReact.CanvasJSChart;

interface ComponentProps {
}

interface ComponentState {
    readings: Array<LightReading>
    isUpdating: boolean
    hours: number
}

interface GraphDataPoint {
    x: Date
    y: number
}

interface GraphData {
    type?: string
    xValueFormatString?: string
    yValueFormatString?: string
    toolTipContent?: string
    dataPoints: Array<GraphDataPoint>
    markerSize?: number
    lineColor?: string
}

export default class LightLevelChart extends Component<ComponentProps, ComponentState> {
    private _isMounted: boolean;

    private selectableHours = [
        {value: 2, text: "Last 2 hours",},
        {value: 6, text: "Last 6 hours",},
        {value: 12, text: "Last 12 hours",},
        {value: 24, text: "Last 24 hours",},
        {value: 36, text: "Last 36 hours",},
        {value: 24 * 2, text: "Last 2 days",},
        {value: 24 * 3, text: "Last 3 days",},
        {value: 24 * 7, text: "Last week",},
        {value: 24 * 7 * 2, text: "Last 2 weeks",},
        {value: Math.round(24 * 30.5), text: "Last month",},
        {value: Math.round(3 * 24 * 30.5), text: "Last 3 months",},
        {value: Math.round(6 * 24 * 30.5), text: "Last 6 months",},
        {value: Math.round(12 * 24 * 30.5), text: "Last year",},
    ]

    constructor(props: ComponentProps) {
        super(props);
        this._isMounted = false;

        this.state = {
            readings: [],
            isUpdating: false,
            hours: 36
        };

        this.update = this.update.bind(this);
        this.onSelectChange = this.onSelectChange.bind(this);
        this.spreadDisruptedDataOverMultipleDatasets = this.spreadDisruptedDataOverMultipleDatasets.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        this.update();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    update() {
        if (!this._isMounted) return;
        this.setState({isUpdating: true});

        api.lightLevels.list(this.state.hours, true)
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then(data => {
                if (!this._isMounted) return;

                if (data.status === "error") {
                    throw new Error(data.data);
                }

                let readings: Array<LightReading> = data.data;
                readings = readings.sort((a, b) => a.created_at.localeCompare(b.created_at));

                this.setState({
                    readings: readings,
                    isUpdating: false,
                });
            })
            .catch(error => {
                if (!this._isMounted) return;

                console.error(`Error fetching lightLevel readings`, error);
                addNotification(new Notification("Error fetching lightLevel readings",
                    error.message,
                    Notification.ERROR));

                this.setState({isUpdating: false,})
            });
    }

    private onSelectChange(e: React.FocusEvent<HTMLSelectElement>) {
        this.setState({
            hours: +e.target.value,
        }, this.update)
    }

    private spreadDisruptedDataOverMultipleDatasets() {
        const data: Array<GraphData> = [];
        this.state.readings
            .filter(it => it.resistance > 0)
            .forEach(it => {
                if (data.length === 0) {
                    data.push(this.createDataSet());
                }

                let lastData = data[data.length - 1];
                const lastDataPoint = lastData.dataPoints[lastData.dataPoints.length - 1];
                const it_created_at_date = new Date(it.created_at);

                if (lastDataPoint !== undefined && it_created_at_date.getTime() - lastDataPoint.x.getTime() > 21 * 60 * 1000) {
                    data.push(this.createDataSet());
                    lastData = data[data.length - 1];
                }

                lastData.dataPoints.push({
                    x: it_created_at_date,
                    y: it.resistance,
                });
            });
        return data;
    }

    private createDataSet() {
        return {
            type: "line",
            xValueFormatString: this.state.hours <= 24 * 3 ? "HH:mm" : "D MMMM",
            yValueFormatString: "#",
            toolTipContent: "{y} Ω at {x}",
            dataPoints: [],
            markerSize: 1,
            lineColor: "#6d78ad",
        };
    }

    render() {
        const options = {
            animationEnabled: true,
            exportEnabled: true,
            theme: "light2", // "light1", "dark1", "dark2",
            zoomEnabled: true,
            zoomType: "xy",
            backgroundColor: null,
            title: {
                text: `Last ${this.state.hours} hours`,
                fontWeight: "normal",
                fontStyle: "italic",
                fontSize: 14,
            },
            axisY: {
                title: "LightLevel",
                includeZero: false,
                suffix: " Ω",
                logarithmic: true,
            },
            axisX: {
                title: "Time",
                prefix: "",
                valueFormatString: this.state.hours <= 24 * 3 ? "HH:mm" : "D MMMM",
            },
            data: this.spreadDisruptedDataOverMultipleDatasets(),
        }

        return <div className={"LightLevelChart"}>
            <LoadingSplash isLoading={this.state.isUpdating}/>

            {this.state.isUpdating ? undefined : <CanvasJSChart options={options}/>}

            <div className={"actions"}>
                <label>Change time span:</label>
                <select onChange={this.onSelectChange}>
                    {this.selectableHours.map(it =>
                        <option selected={this.state.hours === it.value}
                                value={it.value}
                                key={it.value}>
                            {it.text}
                        </option>)}
                </select>
            </div>
        </div>;
    }
}