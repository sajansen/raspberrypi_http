import React, {Component} from 'react';
import {api, throwErrorsIfNotOk} from "../../api";
import {addNotification, Notification} from "../notifications/notifications";
import {Button} from "semantic-ui-react";
import LightLevelRow from './LightLevelRow';
import {LightReading} from "./models";
import LoadingSplash from "../../utils/LoadingSplash";

interface ComponentProps {
}

interface ComponentState {
    readings: Array<LightReading>
    isUpdating: boolean
    hours: number
}

export default class LightLevelList extends Component<ComponentProps, ComponentState> {
    private _isMounted: boolean;

    constructor(props: ComponentProps) {
        super(props);
        this._isMounted = false;

        this.state = {
            readings: [],
            isUpdating: false,
            hours: 12
        };

        this.update = this.update.bind(this);
        this.loadMore = this.loadMore.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        this.update();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    update() {
        if (!this._isMounted) return;
        this.setState({isUpdating: true});

        api.lightLevels.list(this.state.hours, true)
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then(data => {
                if (!this._isMounted) return;

                if (data.status === "error") {
                    throw new Error(data.data);
                }

                const readings: Array<LightReading> = data.data;

                this.setState({
                    readings: readings,
                    isUpdating: false,
                });
            })
            .catch(error => {
                if (!this._isMounted) return;

                console.error(`Error fetching lightLevel readings`, error);
                addNotification(new Notification("Error fetching lightLevel readings",
                    error.message,
                    Notification.ERROR));

                this.setState({isUpdating: false,})
            });
    }

    loadMore() {
        if (this.state.isUpdating) {
            return;
        }

        this.setState({
            hours: this.state.hours + 6
        }, this.update);
    }

    onDelete(reading: LightReading) {
        const result = window.confirm("Are you sure you want to delete this reading?")
        if (!result) {
            return;
        }

        if (!this._isMounted) return;

        api.lightLevels.delete(reading.id)
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then(data => {
                if (!this._isMounted) return;

                if (data.status === "error") {
                    throw new Error(data.data);
                }

                addNotification(new Notification("Light reading deleted",
                    `Reading: ${reading.resistance.toFixed(0)} Ω`,
                    Notification.SUCCESS));

                this.update();
            })
            .catch(error => {
                if (!this._isMounted) return;

                console.error(`Error deleting Light reading`, error);
                addNotification(new Notification("Error deleting light reading",
                    error.message,
                    Notification.ERROR));
            });
    }

    render() {

        return <div className={"LightLevelList"}>
            <LoadingSplash isLoading={this.state.isUpdating}/>

            {this.state.readings.length === 0 ? <h4>Much empty...</h4> : ""}
            <table>
                <tbody>
                {this.state.readings.map(it =>
                    <LightLevelRow key={it.id}
                                    reading={it}
                                    onDelete={this.onDelete}/>)}
                </tbody>
            </table>

            <div className={"actions"}>
                <Button onClick={this.loadMore}>
                    {this.state.isUpdating ? "Loading..." : "Load more"}
                </Button>
            </div>
        </div>;
    }
}