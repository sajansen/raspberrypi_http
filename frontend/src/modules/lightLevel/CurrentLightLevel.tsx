import React, {Component} from 'react';
import {api, throwErrorsIfNotOk} from "../../api";
import {addNotification, Notification} from "../notifications/notifications";
import {Icon} from "semantic-ui-react";
import {LightReading} from "./models";

interface ComponentProps {
}

interface ComponentState {
    reading?: LightReading
    isUpdating: boolean
}

export default class CurrentLightLevel extends Component<ComponentProps, ComponentState> {
    private _isMounted: boolean;

    constructor(props: ComponentProps) {
        super(props);
        this._isMounted = false;

        this.state = {
            isUpdating: false,
        };

        this.update = this.update.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        this.update();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    update() {
        if (!this._isMounted) return;
        this.setState({isUpdating: true});

        api.lightLevels.latest()
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then(data => {
                if (!this._isMounted) return;

                if (data.status === "error") {
                    throw new Error(data.data);
                }

                const reading: LightReading = data.data;

                this.setState({
                    reading: reading,
                    isUpdating: false,
                });
            })
            .catch(error => {
                if (!this._isMounted) return;

                console.error(`Error fetching latest lightLevel reading`, error);
                addNotification(new Notification("Error fetching latest lightLevel reading",
                    error.message,
                    Notification.ERROR));

                this.setState({isUpdating: false,})
            });
    }

    render() {
        if (this.state.isUpdating) {
            return <div className={"CurrentLightLevel"}>
                <Icon loading name="circle notched" title="Fetching latest reading..."/> Loading...
            </div>
        }

        if (this.state.reading === undefined || this.state.reading === null) {
            return <div className={"CurrentLightLevel"}>
                <h4>No latest reading...</h4>
            </div>;
        }

        const lightLevel = (this.state.reading.resistance / 1000.0)
            .toFixed(3)
            .split(".");
        return <div className={"CurrentLightLevel"}>
            <h1>
                {lightLevel[0]}
                <small>
                    {lightLevel[1]}
                </small>
                &nbsp;Ω
            </h1>
        </div>;
    }
}