import React, {Component} from 'react';
import {api, throwErrorsIfNotOk} from "../../api";
import {addNotification, Notification} from "../notifications/notifications";
import {CronJob} from "./models";
import CronJobsRow from "./CronJobsRow";
import {Icon} from "semantic-ui-react";

interface ComponentProps {
    onEdit: (cronjob: CronJob) => void,
}

interface ComponentState {
    cronjobs: Array<CronJob>,
    isUpdating: boolean,
}

export default class CronJobsTable extends Component<ComponentProps, ComponentState> {
    private _isMounted: boolean;

    public static defaultProps = {
        onEdit: () => null,
    }

    constructor(props: ComponentProps) {
        super(props);
        this._isMounted = false;

        this.state = {
            cronjobs: [],
            isUpdating: false,
        };

        this.update = this.update.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        this.update();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    update() {
        if (!this._isMounted) return;
        this.setState({isUpdating: true});

        api.cronjobs.list()
            .then(throwErrorsIfNotOk)
            .then(response => response.json())
            .then(data => {
                if (!this._isMounted) return;

                if (data.status === "error") {
                    throw new Error(data.data);
                }

                const cronjobs = data.data.map((it: CronJob) =>
                    new CronJob(it.id,
                        it.name,
                        it.script,
                        it.execute_weekdays,
                        it.execute_time.split(":", 2).join(":"),    // Convert 00:00:00 to 00:00
                        it.enabled));

                this.setState({
                    cronjobs: cronjobs,
                    isUpdating: false,
                });
            })
            .catch(error => {
                if (!this._isMounted) return;

                console.error(`Error fetching cronjobs`, error);
                addNotification(new Notification("Error fetching cronjobs",
                    error.message,
                    Notification.ERROR));

                this.setState({isUpdating: false,})
            });
    }

    render() {
        if (this.state.isUpdating) {
            return <div className={"CronJobsTable"}>
                <Icon loading name="circle notched"
                      size="huge"
                      title="Fetching cronjobs..."/>
            </div>
        }

        return <table className={"CronJobsTable"}>
            <tbody>
            {this.state.cronjobs.length === 0 ? <tr><td>Much empty...</td></tr> : ""}
            {this.state.cronjobs.map(it =>
                <CronJobsRow key={it.id + it.name} cronjob={it} onUpdate={this.update} onEdit={this.props.onEdit}/>)}
            </tbody>
        </table>;
    }
}