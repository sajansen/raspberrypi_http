import React, {Component} from 'react';
import CronJobsTable from "./CronJobsTable";
import {CronJob} from "./models";
import CronJobsForm from "./CronJobsForm";
import "./cronjobs.sass";
import MQTTStatus from "../mqtt/MQTTStatus";

interface ComponentProps {
}

interface ComponentState {
    editCronjob: CronJob | undefined,
    newCronjob: boolean,
}

export default class CronJobs extends Component<ComponentProps, ComponentState> {

    private readonly tableComp: React.RefObject<CronJobsTable>;

    constructor(props: ComponentProps) {
        super(props);

        this.tableComp = React.createRef();

        this.state = {
            editCronjob: undefined,
            newCronjob: false,
        };

        this.editCronjob = this.editCronjob.bind(this);
        this.newCronjob = this.newCronjob.bind(this);
        this.cronjobSaved = this.cronjobSaved.bind(this);
        this.closeCronjobForm = this.closeCronjobForm.bind(this);
    }

    editCronjob(cronjob: CronJob) {
        this.closeCronjobForm(() => this.setState({
            editCronjob: cronjob,
        }));
    }

    newCronjob() {
        this.closeCronjobForm(() => this.setState({
            newCronjob: true,
        }));
    }

    cronjobSaved() {
        this.closeCronjobForm();
        this.tableComp.current?.update()
    }

    closeCronjobForm(callback?: () => void) {
        this.setState({
            editCronjob: undefined,
            newCronjob: false,
        }, () => typeof callback !== "function" ? "" : callback());
    }

    render() {
        return <div className={"CronJobs"}>
            <h1>Cronjobs</h1>

            <button onClick={this.newCronjob}>New</button>

            <CronJobsTable ref={this.tableComp}
                           onEdit={this.editCronjob}/>

            {this.state.editCronjob === undefined && !this.state.newCronjob ? "" :
                <CronJobsForm cronjob={this.state.editCronjob}
                              onCancel={this.closeCronjobForm}
                              onSaved={this.cronjobSaved}/>}

            <MQTTStatus/>
        </div>;
    }
}