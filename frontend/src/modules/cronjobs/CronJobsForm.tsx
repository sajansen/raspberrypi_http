import React, {Component} from 'react';
import {CronJob} from "./models";
import {api, throwErrorsIfNotOk} from "../../api";
import {addNotification, Notification} from "../notifications/notifications";

interface ComponentProps {
    cronjob: CronJob | undefined,
    onSaved: () => void,
    onCancel: () => void,
}

interface ComponentState {
}

export default class CronJobsForm extends Component<ComponentProps, ComponentState> {
    public static defaultProps = {
        onSaved: () => null,
        onCancel: () => null,
    }

    constructor(props: ComponentProps) {
        super(props);

        this.state = {};

        this.submit = this.submit.bind(this);
    }

    submit(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();

        const htmlFormData = new FormData(e.target as HTMLFormElement);

        const cronjob = new CronJob(
            this.props.cronjob?.id,
            htmlFormData.get("name") as string,
            htmlFormData.get("script") as string,
            htmlFormData.getAll("execute_weekdays[]") as Array<string>,
            htmlFormData.get("execute_time") as string,
            htmlFormData.get("enabled") === "on",
        )

        if (this.props.cronjob === undefined) {
            api.cronjobs.add(cronjob)
                .then(throwErrorsIfNotOk)
                .then(response => response.json())
                .then(data => {
                    if (data.status === "error") {
                        throw new Error(data.data);
                    }

                    console.log("Cronjob created");
                    this.props.onSaved();
                })
                .catch(error => {
                    console.error(`Error adding new cronjob`, error);
                    addNotification(new Notification("Error adding new cronjob",
                        error.message,
                        Notification.ERROR));
                });
        } else {
            api.cronjobs.save(this.props.cronjob.id, cronjob)
                .then(throwErrorsIfNotOk)
                .then(response => response.json())
                .then(data => {
                    if (data.status === "error") {
                        throw new Error(data.data);
                    }

                    console.log("Cronjob updated");
                    this.props.onSaved();
                })
                .catch(error => {
                    console.error(`Error updating cronjob`, error);
                    addNotification(new Notification("Error updating cronjob",
                        error.message,
                        Notification.ERROR));
                });
        }
    }

    render() {
        return <form className={"CronJobsForm"} onSubmit={this.submit}>
            <input type="hidden" name="id" defaultValue={this.props.cronjob?.id || undefined}/>
            <div className="form-group">
                <label htmlFor="name">Name</label>
                <input type="text" name="name" maxLength={255} required defaultValue={this.props.cronjob?.name}/>
            </div>
            <div className="form-group">
                <label htmlFor="script">Script</label>
                <textarea name="script" required defaultValue={this.props.cronjob?.script}/>
            </div>
            <div className="form-group">
                <label htmlFor="execute_weekdays[]">Weekdays</label>
                <div className="form-weekdays">
                    <label>
                        <input type="checkbox" name="execute_weekdays[]"
                               defaultValue="sunday" defaultChecked={this.props.cronjob?.execute_weekdays.indexOf("sunday") !== -1}/>
                        Sunday
                    </label>
                    <label>
                        <input type="checkbox" name="execute_weekdays[]"
                               defaultValue="monday" defaultChecked={this.props.cronjob?.execute_weekdays.indexOf("monday") !== -1}/>
                        Monday
                    </label>
                    <label>
                        <input type="checkbox" name="execute_weekdays[]"
                               defaultValue="tuesday" defaultChecked={this.props.cronjob?.execute_weekdays.indexOf("tuesday") !== -1}/>
                        Tuesday
                    </label>
                    <label>
                        <input type="checkbox" name="execute_weekdays[]"
                               defaultValue="wednesday" defaultChecked={this.props.cronjob?.execute_weekdays.indexOf("wednesday") !== -1}/>
                        Wednesday
                    </label>
                    <label>
                        <input type="checkbox" name="execute_weekdays[]"
                               defaultValue="thursday" defaultChecked={this.props.cronjob?.execute_weekdays.indexOf("thursday") !== -1}/>
                        Thursday
                    </label>
                    <label>
                        <input type="checkbox" name="execute_weekdays[]"
                               defaultValue="friday" defaultChecked={this.props.cronjob?.execute_weekdays.indexOf("friday") !== -1}/>
                        Friday
                    </label>
                    <label>
                        <input type="checkbox" name="execute_weekdays[]"
                               defaultValue="saturday" defaultChecked={this.props.cronjob?.execute_weekdays.indexOf("saturday") !== -1}/>
                        Saturday
                    </label>
                </div>
            </div>
            <div className="form-group">
                <label htmlFor="execute_time">Execute at</label>
                <input type="time" name="execute_time" required defaultValue={this.props.cronjob?.execute_time}/>
            </div>
            <div className="form-group">
                <label htmlFor="enabled">Enable</label>
                <input type="checkbox" name="enabled" defaultChecked={this.props.cronjob?.enabled}/>
            </div>

            <div className="form-group">
                <label/>
                <button type="submit">Save</button>
                <button type="reset" onClick={this.props.onCancel}>Cancel</button>
            </div>
        </form>;
    }
}