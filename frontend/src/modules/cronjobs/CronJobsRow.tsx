import React, {Component} from 'react';
import {CronJob} from "./models";
import {api, throwErrorsIfNotOk} from "../../api";
import {addNotification, Notification} from "../notifications/notifications";

interface ComponentProps {
    cronjob: CronJob,
    onUpdate: () => void,
    onEdit: (cronjob: CronJob) => void,
}

interface ComponentState {
}

export default class CronJobsRow extends Component<ComponentProps, ComponentState> {
    public static defaultProps = {
        onUpdate: () => null,
        onEdit: () => null,
    }

    constructor(props: ComponentProps) {
        super(props);

        this.state = {};

        this.edit = this.edit.bind(this);
        this.delete = this.delete.bind(this);
        this.execute = this.execute.bind(this);
    }

    edit() {
        console.debug("Editing cronjob", this.props.cronjob);
        this.props.onEdit(this.props.cronjob);
    }

    delete() {
        console.log("Deleting cronjob", this.props.cronjob);

        console.debug("Asking confirmation for Cronjob action");
        const confirmChoice = window.confirm(`Are you sure you want to delete cronjob: ${this.props.cronjob.name}?`);
        if (!confirmChoice) {
            return;
        }

        api.cronjobs.delete(this.props.cronjob.id)
            .then(throwErrorsIfNotOk)
            .then(r => r.json())
            .then((data) => {
                if (data.status === "error") {
                    throw new Error(data.data);
                }

                addNotification(new Notification(this.props.cronjob.name, "deleted", Notification.SUCCESS));

                this.props.onUpdate();
            })
            .catch(error => {
                console.error(`Error deleting cronjob: ${this.props.cronjob.name}`, error);
                addNotification(new Notification("Error deleting cronjob", error.message, Notification.ERROR));
            });
    }

    execute() {
        console.log("Executing cronjob", this.props.cronjob);

        api.cronjobs.execute(this.props.cronjob.id)
            .then(throwErrorsIfNotOk)
            .then(r => r.json())
            .then((data) => {
                if (data.status === "error") {
                    throw new Error(data.data);
                }

                addNotification(new Notification(this.props.cronjob.name, "executed", Notification.SUCCESS));
            })
            .catch(error => {
                console.error(`Error executing cronjob: ${this.props.cronjob.name}`, error);
                addNotification(new Notification("Error executing cronjob", error.message, Notification.ERROR));
            });
    }

    render() {
        return <tr className={`CronJobsRow ${this.props.cronjob.enabled ? "" : "disabled"}`}>
            <td>
                <a title="Edit cronjob"
                   onClick={this.edit}>{this.props.cronjob.name}</a>
            </td>

            <td>
                <div className="weekdays-first-letter-list">
                    {CronJob.allowedWeekdays.map(weekday =>
                        <span title={weekday} key={weekday}>
                            {this.props.cronjob.execute_weekdays.indexOf(weekday) > -1 ? weekday[0].toUpperCase() : ""}
                        </span>)}
                </div>
            </td>

            <td>{this.props.cronjob.execute_time}</td>

            <td className="cronjob-actions">
                <a onClick={this.delete}
                   className="a-symbol"
                   title="Delete cronjob">&#128465;</a> /

                <a onClick={this.execute}
                   className="a-symbol"
                   title="Execute cronjob right now">&#9654;</a>
            </td>
        </tr>;
    }
}