export class CronJob {

    static allowedWeekdays = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"];

    id: number | null;
    name: string;
    script: string;
    execute_weekdays: Array<string>;
    execute_time: string;
    enabled: boolean;

    constructor(id: number | null | undefined,
                name: string,
                script: string,
                execute_weekdays: Array<string>,
                execute_time: string,
                enabled: boolean
    ) {
        this.id = id || null
        this.name = name
        this.script = script
        this.execute_weekdays = execute_weekdays
        this.execute_time = execute_time
        this.enabled = enabled
    }
}