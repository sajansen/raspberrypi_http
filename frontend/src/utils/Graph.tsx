import React, {Component} from 'react';
import App from "../App";

interface ComponentProps {
    data: Array<Array<number>>
    width: number
    height: number
    margin: number
    fitYAxis: boolean
    title: string
}

interface ComponentState {
    hoveredIndex: number | undefined
}

export default class Graph extends Component<ComponentProps, ComponentState> {
    static defaultProps = {
        margin: 3,
        fitYAxis: false,
        title: "",
    }

    private readonly canvasComp: React.RefObject<HTMLCanvasElement>;
    private _isMounted: boolean;
    private _context: CanvasRenderingContext2D | null | undefined;

    constructor(props: ComponentProps) {
        super(props);
        this._isMounted = false;

        this.canvasComp = React.createRef();

        this.state = {
            hoveredIndex: undefined,
        };

        this.repaint = this.repaint.bind(this);
        this.onMouseMove = this.onMouseMove.bind(this);
        this.onMouseLeave = this.onMouseLeave.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        this._context = this.canvasComp.current?.getContext("2d");
        this.repaint();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    onMouseMove(e: React.MouseEvent<HTMLCanvasElement>) {
        if (this.canvasComp.current == null || this._context == null) {
            this.setState({
                hoveredIndex: undefined,
            });
            return;
        }

        const x = e.clientX - this.canvasComp.current.getBoundingClientRect().left - this.props.margin;
        if (x < 0 || x > this.props.width) {
            this.onMouseLeave();
            return;
        }

        const minX = Math.min.apply(Math, this.props.data.map(it => it[0]));
        const maxX = Math.max.apply(Math, this.props.data.map(it => it[0]));

        const timestampCursor = minX + x / this.props.width * (maxX - minX);
        let maxHoverIndex = this.props.data.findIndex(it => it[0] > timestampCursor);
        if (maxHoverIndex < 0 || maxHoverIndex >= this.props.data.length) {
            this.onMouseLeave();
            return;
        }
        const minHoverIndex = Math.max(0, maxHoverIndex - 1);

        const statsCenter = this.props.data[minHoverIndex][0] + (this.props.data[maxHoverIndex][0] - this.props.data[minHoverIndex][0]) / 2;
        let statHoverIndex;
        if (timestampCursor < statsCenter) {
            statHoverIndex = minHoverIndex;
        } else {
            statHoverIndex = maxHoverIndex;
        }

        this.setState({
            hoveredIndex: statHoverIndex,
        }, this.repaint);
    }

    onMouseLeave() {
        this.setState({
            hoveredIndex: undefined,
        }, this.repaint);
    }

    repaint() {
        if (!this._context) {
            return;
        }

        // Clear canvas
        this._context.clearRect(0, 0, this.props.width + 2 * this.props.margin, this.props.height + 2 * this.props.margin);

        this.paintAxis();

        // Graph data
        if (this.props.data.length === 0) {
            return;
        }

        const lowestValue = !this.props.fitYAxis ? 0 : Math.min.apply(Math, this.props.data.map(it => it[1]));
        const highestValue = Math.max(10, Math.max.apply(Math, this.props.data.map(it => it[1])));
        const horizontalScale = this.props.width / (this.props.data.length - 1);

        this.paintGrid(lowestValue, highestValue);

        this.paintStatPoints(lowestValue, highestValue);

        this.paintHoveredStatInfo(horizontalScale);
    }

    private paintStatPoints(lowestValue: number, highestValue: number) {
        if (!this._context) {
            return;
        }

        const minX = Math.min.apply(Math, this.props.data.map(it => it[0]));
        const maxX = Math.max.apply(Math, this.props.data.map(it => it[0]));

        this._context.beginPath();
        this._context.strokeStyle = App.isNightTime() ? `rgb(255, 255, 255)` : `rgb(0, 200, 0)`;
        this._context.lineWidth = 1;

        this.props.data.forEach(stat => {
            const x = this.props.width * ((stat[0] - minX) / (maxX - minX));
            const y = highestValue === 0
                ? this.props.height
                : this.props.height - this.props.height / (highestValue - lowestValue) * (stat[1] - lowestValue);
            this._context!.arc(this.props.margin + x, this.props.margin + y, 0.5, 0, 2 * Math.PI);
        });

        this._context.stroke();
    }

    private paintHoveredStatInfo(horizontalScale: number) {
        if (!this._context) {
            return;
        }

        if (this.state.hoveredIndex === undefined) {
            return;
        }

        let stat = this.props.data[this.state.hoveredIndex];
        const minX = Math.min.apply(Math, this.props.data.map(it => it[0]));
        const maxX = Math.max.apply(Math, this.props.data.map(it => it[0]));
        const x = this.props.width * ((stat[0] - minX) / (maxX - minX));

        this._context.beginPath();
        this._context.strokeStyle = App.isNightTime() ? `rgb(0, 200, 0)` : `rgb(120, 120, 120)`;
        this._context.fillStyle = this._context.strokeStyle;
        this._context.moveTo(this.props.margin + x, this.props.margin);
        this._context.lineTo(this.props.margin + x, this.props.margin + this.props.height);

        this._context.font = "12px Arial";
        this._context!.fillText(stat[1].toString(), this.props.margin + 5, this.props.margin + 8);
        this._context.stroke();
    }

    private paintAxis() {
        if (!this._context) {
            return;
        }

        this._context.beginPath();
        this._context.strokeStyle = App.isNightTime() ? `rgb(0, 255, 0)` : `rgb(80, 80, 80)`;
        this._context.lineWidth = 1;
        this._context.moveTo(this.props.margin, this.props.margin * 0.5);
        this._context.lineTo(this.props.margin, this.props.margin + this.props.height);
        this._context.lineTo(this.props.margin * 1.5 + this.props.width, this.props.margin + this.props.height);
        this._context.stroke();
    }

    private paintGrid(lowestValue: number, highestValue: number) {
        if (!this._context) {
            return;
        }

        let interval;
        if (highestValue - lowestValue >= 2000) {
            interval = 1000;
        } else if (highestValue - lowestValue >= 1000) {
            interval = 500;
        } else if (highestValue - lowestValue >= 200) {
            interval = 100;
        } else if (highestValue - lowestValue >= 50) {
            interval = 25;
        } else if (highestValue - lowestValue >= 10) {
            interval = 10;
        } else {
            return;
        }

        this._context.strokeStyle = App.isNightTime() ? `rgb(0, 255, 0)` : `rgb(80, 80, 80)`;
        this._context.lineWidth = 1;
        let y = interval;
        while (y <= highestValue) {
            this._context.beginPath();
            const thisY = this.props.height - this.props.height / (highestValue - lowestValue) * (y - lowestValue);
            this._context.moveTo(this.props.margin, this.props.margin + thisY);
            this._context.lineTo(this.props.margin + 2, this.props.margin + thisY);
            this._context.stroke();

            y += interval;
        }
    }

    render() {
        return <canvas ref={this.canvasComp}
                       title={this.props.title}
                       onMouseMove={this.onMouseMove}
                       onMouseLeave={this.onMouseLeave}
                       width={this.props.width + 2 * this.props.margin}
                       height={this.props.height + 2 * this.props.margin}/>;
    }
}