
## Install

```bash
pip install -r requirements

# Set Flask environment
#FLASK_APP=app.py

# Run server
flask run

```


### Database
Source: https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database

```bash
# Create database
flask db init

# Create new migration file
flask db migrate -m <comments>

# Apply database migrations
flask db upgrade


```

### Raspberry PI

When running into troubles with an existing global installation of Flask, and unable to remove this:

```bash
$  pip3 install --upgrade Flask==1.1.1
$  . venv/bin/activate
$  export FLASK_APP=$(pwd)/app.py
$  flask run
```

## Update

```bash
cd raspberrypi_http
git pull
sudo systemctl restart raspberry_http.service
```


# Modules

## Download stats

### Setup

#### Specify repositories

Open [config.py](config.py) and edit the repositories in ```DOWNLOAD_STATS_GITHUB_REPOS``` and ```DOWNLOAD_STATS_BITBUCKET_REPOS```. Also make sure the usernames for GitHub and BitBucket are set right. 

#### Scheduled collection run

Add this line to your crontab to collect download statistics every midnight (and save output to system logger):
```
0 0 * * * curl --insecure --silent http://127.0.0.1:5000/api/v1/downloads/stats/collect -X POST | /usr/bin/logger -t raspberrypi_http_downloadstats
```

