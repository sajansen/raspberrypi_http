from sqlalchemy import Column, Integer, DateTime, Float

from models import Serializable
from modules.analog_sensors.db import Base
from app import db


class TemperatureReading(Base, Serializable):
    __tablename__ = 'temp_readings'

    id = Column(Integer, primary_key=True)
    temperature = Column(Float, nullable=False)
    created_at = Column(DateTime, nullable=False)

    def __repr__(self):
        return "TempReading(id={}, temperature={}, created_at={})" \
            .format(self.id, self.temperature, self.created_at)

    def serialize(self):
        return {
            "id": self.id,
            "temperature": self.temperature,
            "created_at": self.created_at,
        }


class LightReading(Base, Serializable):
    __tablename__ = 'light_readings'

    id = Column(Integer, primary_key=True)
    raw_value = Column(Float, nullable=False)
    resistance = Column(Float, nullable=False)
    created_at = Column(DateTime, nullable=False)

    def __repr__(self):
        return "LightReading(id={}, raw_value={}, resistance={}, created_at={})" \
            .format(self.id, self.raw_value, self.resistance, self.created_at)

    def serialize(self):
        return {
            "id": self.id,
            "raw_value": self.raw_value,
            "resistance": self.resistance,
            "created_at": self.created_at,
        }


# The pulses
class ElectricityReading(db.Model, Serializable):
    __tablename__ = 'electricity_readings'

    id = Column(Integer, primary_key=True)
    pulses = Column(Integer, nullable=False)
    created_at = Column(DateTime, nullable=False)

    def __repr__(self):
        return "ElectricityReading(id={}, pulses={}, created_at={})" \
            .format(self.id, self.pulses, self.created_at)

    def serialize(self):
        return {
            "id": self.id,
            "pulses": self.pulses,
            "created_at": self.created_at,
        }


# An absolute reading of the full digit scale
class ElectricityAbsoluteReading(db.Model, Serializable):
    __tablename__ = 'electricity_absolute_readings'

    id = Column(Integer, primary_key=True)
    reading = Column(Float, nullable=False)
    timestamp = Column(DateTime, nullable=False)

    def __repr__(self):
        return "ElectricityAbsoluteReading(id={}, reading={}, timestamp={})" \
            .format(self.id, self.reading, self.timestamp)

    def serialize(self):
        return {
            "id": self.id,
            "reading": self.reading,
            "timestamp": self.timestamp,
        }
