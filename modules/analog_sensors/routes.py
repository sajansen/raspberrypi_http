import logging
from datetime import datetime, timedelta

from flask import request

from models import ElectricityAbsoluteReading, ApiResponse
from modules.analog_sensors import db
from app import db as app_db
from modules.analog_sensors.models import TemperatureReading, LightReading, ElectricityReading
from routes_api import API_URL
from app import app

logger = logging.getLogger(__name__)

BASE_URL = API_URL + "/sensors"


@app.route(BASE_URL + "/temperatures", methods=['GET'])
def api_temperature_all():
    try:
        hours = int(request.args.get('hours'))
    except:
        hours = 24

    sort_order_desc = request.args.get('order') is not None and request.args.get('order').lower() == "desc"

    try:
        db.connect()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to connect to database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    logger.info("Querying database")
    try:
        session = db.new_session()
        results = session.query(TemperatureReading) \
            .filter(TemperatureReading.created_at >= datetime.now() - timedelta(hours=hours))

        if sort_order_desc:
            results = results.order_by(TemperatureReading.created_at.desc())
        else:
            results = results.order_by(TemperatureReading.created_at.asc())

        results = results.all()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to read from database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    return ApiResponse.json(results)


@app.route(BASE_URL + '/temperatures/latest', methods=['GET'])
def api_temperature_latest():
    try:
        db.connect()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to connect to database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    logger.info("Querying database")
    try:
        session = db.new_session()
        result = session.query(TemperatureReading) \
            .order_by(TemperatureReading.created_at.desc()) \
            .first()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to read from database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    return ApiResponse.json(result)


@app.route(BASE_URL + '/temperatures/<int:id>', methods=['DELETE'])
def api_temperature_delete(id: int):
    try:
        db.connect()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to connect to database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    try:
        session = db.new_session()
        entity = session.query(TemperatureReading).get(id)
        logger.info("Removing entity from database: {}".format(entity))
        session.delete(entity)
        session.commit()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to delete from database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    return ApiResponse.json(entity)


@app.route(BASE_URL + "/light-levels", methods=['GET'])
def api_light_levels_all():
    try:
        hours = int(request.args.get('hours'))
    except:
        hours = 24

    sort_order_desc = request.args.get('order') is not None and request.args.get('order').lower() == "desc"

    try:
        db.connect()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to connect to database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    logger.info("Querying database")
    try:
        session = db.new_session()
        results = session.query(LightReading) \
            .filter(LightReading.created_at >= datetime.now() - timedelta(hours=hours))

        if sort_order_desc:
            results = results.order_by(LightReading.created_at.desc())
        else:
            results = results.order_by(LightReading.created_at.asc())

        results = results.all()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to read from database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    return ApiResponse.json(results)


@app.route(BASE_URL + '/light-levels/latest', methods=['GET'])
def api_light_levels_latest():
    try:
        db.connect()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to connect to database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    logger.info("Querying database")
    try:
        session = db.new_session()
        result = session.query(LightReading) \
            .order_by(LightReading.created_at.desc()) \
            .first()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to read from database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    return ApiResponse.json(result)


@app.route(BASE_URL + '/light-levels/<int:id>', methods=['DELETE'])
def api_light_level_delete(id: int):
    try:
        db.connect()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to connect to database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    try:
        session = db.new_session()
        entity = session.query(LightReading).get(id)
        logger.info("Removing entity from database: {}".format(entity))
        session.delete(entity)
        session.commit()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to delete from database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    return ApiResponse.json(entity)


@app.route(BASE_URL + '/electricity', methods=['GET'])
def api_electricity_all():
    try:
        hours = int(request.args.get('hours'))
    except:
        hours = 24

    sort_order_desc = request.args.get('order') is not None and request.args.get('order').lower() == "desc"

    logger.info("Querying database")
    try:
        results = (ElectricityReading.query
                   .filter(ElectricityReading.created_at >= datetime.now() - timedelta(hours=hours)))

        if sort_order_desc:
            results = results.order_by(ElectricityReading.created_at.desc())
        else:
            results = results.order_by(ElectricityReading.created_at.asc())

        results = results.all()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to read ElectricityReading from database: " + str(e),
                                status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    return ApiResponse.json(results)


@app.route(BASE_URL + '/electricity/reading', methods=['POST'])
def api_electricity_new():
    data = request.json
    obj = ElectricityReading(pulses=data['pulse_count'], created_at=datetime.now())
    logger.info("Creating new electricity reading in database: {}".format(obj))

    try:
        app_db.session.add(obj)
        app_db.session.commit()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to add reading to database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    return ApiResponse.json(obj)


@app.route(BASE_URL + '/electricity/absolute', methods=['GET'])
def api_electricity_absolute_all():
    try:
        hours = int(request.args.get('hours'))
    except:
        hours = 24

    sort_order_desc = request.args.get('order') is not None and request.args.get('order').lower() == "desc"

    logger.info("Querying database")
    try:
        results = (ElectricityAbsoluteReading.query
                   .filter(ElectricityAbsoluteReading.timestamp >= datetime.now() - timedelta(hours=hours)))

        if sort_order_desc:
            results = results.order_by(ElectricityAbsoluteReading.timestamp.desc())
        else:
            results = results.order_by(ElectricityAbsoluteReading.timestamp.asc())

        results = results.all()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to read ElectricityAbsoluteReading from database: " + str(e),
                                status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    return ApiResponse.json(results)


@app.route(BASE_URL + '/electricity/absolute', methods=['POST'])
def api_electricity_absolute_new():
    data = request.json
    timestamp = datetime.strptime(data["timestamp"], '%Y-%m-%d %H:%M')
    obj = ElectricityAbsoluteReading(reading=data['reading'], timestamp=timestamp)
    logger.info("Creating new electricity absolute reading in database: {}".format(obj))

    try:
        app_db.session.add(obj)
        app_db.session.commit()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to add absolute reading to database: " + str(e),
                                status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    return ApiResponse.json(obj)


@app.route(BASE_URL + '/electricity/absolute/<int:id>', methods=['DELETE'])
def api_electricity_absolute_delete(id: int):
    try:
        obj = ElectricityAbsoluteReading.query.get(id)
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to get ElectricityAbsoluteReading from database: " + str(e),
                                status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    try:
        app_db.session.delete(obj)
        app_db.session.commit()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to delete absolute reading from database: " + str(e),
                                status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    return ApiResponse.json(obj)
