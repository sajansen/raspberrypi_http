import logging
import subprocess

from models import ApiResponse
from routes_api import API_URL
from app import app

logger = logging.getLogger(__name__)

BASE_URL = API_URL + "/system"


@app.route(BASE_URL + "/motion/start", methods=['GET'])
def api_motion_start():
    result = subprocess.run(["systemctl", "restart", "motion"], stdout=subprocess.PIPE)
    return ApiResponse.json('{}'.format(result.stdout),
                     status=ApiResponse.RESPONSE_OK if result.returncode == 0 else ApiResponse.RESPONSE_ERRORS,
                     response_code=200 if result.returncode == 0 else 500)


@app.route(BASE_URL + "/motion/stop", methods=['GET'])
def api_motion_stop():
    result = subprocess.run(["systemctl", "stop", "motion"], stdout=subprocess.PIPE)
    return ApiResponse.json('{}'.format(result.stdout),
                     status=ApiResponse.RESPONSE_OK if result.returncode == 0 else ApiResponse.RESPONSE_ERRORS,
                     response_code=200 if result.returncode == 0 else 500)
