import enum

from sqlalchemy import Column, Integer, DateTime, String, Enum

from models import Serializable
from modules.analog_sensors.db import Base


class Room(enum.Enum):
    Unknown = -1
    Downstairs = 0
    FirstFloor = 1


class Connection(Base, Serializable):
    __tablename__ = 'connections'

    id = Column(Integer, primary_key=True)
    device_name = Column(String)
    mac_address = Column(String)
    ip = Column(String)
    interface = Column(String)
    room = Column(Enum(Room))
    connected_at = Column(DateTime, nullable=False)
    disconnected_at = Column(DateTime, nullable=True)

    def __repr__(self):
        return "Connection(id={}, device_name={}, mac_address={}, " \
               "ip={}, interface={}, room={}, connected_at={}, disconnected_at={})" \
            .format(self.id,
                    self.device_name,
                    self.mac_address,
                    self.ip,
                    self.interface,
                    self.room.name,
                    self.connected_at,
                    self.disconnected_at, )

    def serialize(self):
        return {
            "id": self.id,
            "device_name": self.device_name,
            "mac_address": self.mac_address,
            "ip": self.ip,
            "interface": self.interface,
            "room": self.room,
            "connected_at": self.connected_at,
            "disconnected_at": self.disconnected_at,
        }
