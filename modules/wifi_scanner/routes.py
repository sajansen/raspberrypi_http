import logging

from models import ApiResponse
from modules.wifi_scanner import db
from modules.wifi_scanner.models import Connection
from routes_api import API_URL
from app import app

logger = logging.getLogger(__name__)

BASE_URL = API_URL + "/wifi-scanner"


@app.route(BASE_URL + "", methods=['GET'])
def api_wifi_scanner_all():
    try:
        db.connect()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to connect to database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    logger.info("Querying database")
    try:
        session = db.new_session()
        results = session.query(Connection) \
            .order_by(Connection.device_name.asc()) \
            .order_by(Connection.ip.asc()) \
            .all()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to read from database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    return ApiResponse.json(results)


@app.route(BASE_URL + "/devices", methods=['GET'])
def api_wifi_scanner_devices():
    try:
        db.connect()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to connect to database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    logger.info("Querying database")
    try:
        session = db.new_session()
        results = session.query(Connection) \
            .group_by(Connection.mac_address) \
            .order_by(Connection.device_name.asc()) \
            .order_by(Connection.ip.asc()) \
            .all()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to read from database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    return ApiResponse.json(results)


@app.route(BASE_URL + '/<int:id>', methods=['DELETE'])
def api_wifi_scanner_delete(id: int):
    try:
        db.connect()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to connect to database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    try:
        session = db.new_session()
        entity = session.query(Connection).get(id)
        logger.info("Removing entity from database: {}".format(entity))
        session.delete(entity)
        session.commit()
    except Exception as e:
        logger.exception(e, stack_info=True)
        return ApiResponse.json("Failed to delete from database: " + str(e), status=ApiResponse.RESPONSE_ERRORS,
                                response_code=500)

    return ApiResponse.json(entity)
