import logging

from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from config import Config

logger = logging.getLogger(__name__)

Base = declarative_base()
_engine = None
Session = None


def connect():
    global _engine, Session
    logger.info("Connecting to database: {}".format(Config.wifi_scanner_db))
    _engine = create_engine(Config.wifi_scanner_db, echo=True)
    Session = sessionmaker(bind=_engine)


def new_session():
    return Session()
