import datetime
import enum

from flask.json import JSONEncoder

from models import Serializable


class MyJSONEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, (datetime.date, datetime.datetime, datetime.time)):
            return obj.isoformat()
        if isinstance(obj, enum.Enum):
            return obj.name
        if isinstance(obj, Serializable):
            return obj.serialize()
        return super(MyJSONEncoder, self).default(obj)
