import logging

from flask import request

from app import app
from config import Config
from models import CronJob, ApiResponse, DownloadStat
from mqtt_connection import mqtt_client
from scripts import download_stats
from scripts.cronjob import get_current_cronjobs, get_scripts_for_cronjobs, save_cronjob_from_request, delete_cronjob, \
    execute_current_cronjobs, save_cronjob

logger = logging.getLogger(__name__)

API_URL = '/api/v1'


######################
#
#   Cronjobs
#

@app.route(API_URL + '/cronjobs', methods=['GET'])
def api_cronjobs_all():
    cronjobs = CronJob.query.order_by(CronJob.execute_time.asc(), CronJob.name.asc()).all()
    return ApiResponse.json(cronjobs)


@app.route(API_URL + '/cronjobs/now', methods=['GET'])
def api_cronjobs_get_current():
    cronjobs = get_current_cronjobs().all()
    return ApiResponse.json(cronjobs)


@app.route(API_URL + '/cronjobs/now/scripts', methods=['GET'])
def api_cronjobs_get_current_raw_script():
    scripts = get_scripts_for_cronjobs(get_current_cronjobs())
    return ApiResponse.json(scripts)


@app.route(API_URL + '/cronjobs/now/execute', methods=['POST'])
def api_cronjobs_execute_current():
    executed = execute_current_cronjobs()
    return ApiResponse.json("{} cronjobs executed".format(executed))


@app.route(API_URL + "/cronjobs/add", methods=['POST', "PUT"])
def api_cronjobs_add():
    json = request.json
    if json is None:
        return ApiResponse.json("No valid JSON object received", status=ApiResponse.RESPONSE_ERRORS)

    cronjob = CronJob()
    cronjob.from_json(json)

    validation_results = save_cronjob(cronjob)
    if validation_results is not None:
        logger.warning(validation_results)

    return ApiResponse.json(cronjob)


@app.route(API_URL + "/cronjobs/<int:id>/save", methods=['POST'])
def api_cronjobs_save(id: int):
    json = request.json
    if json is None:
        return ApiResponse.json("No valid JSON object received", status=ApiResponse.RESPONSE_ERRORS)

    cronjob = CronJob.query.get(id)
    cronjob.from_json(json)

    validation_results = save_cronjob(cronjob)
    if validation_results is not None:
        logger.warning(validation_results)
        return ApiResponse.json(validation_results, status=ApiResponse.RESPONSE_ERRORS)

    return ApiResponse.json(cronjob)


@app.route(API_URL + '/cronjobs/<int:id>/execute', methods=['POST'])
def api_cronjobs_execute(id: int):
    cronjob = CronJob.query.get_or_404(id)
    cronjob.execute()
    return ApiResponse.json(cronjob)


@app.route(API_URL + "/cronjobs/<int:id>/delete", methods=["DELETE", "POST"])
def api_cronjobs_delete(id: int):
    cronjob = CronJob.query.get(id)
    delete_cronjob(cronjob)

    return ApiResponse.json()


######################
#
#   MQTT
#

@app.route(API_URL + "/mqtt/status", methods=['GET'])
def api_mqtt_status():
    return ApiResponse.json({
        "isConnected": mqtt_client.is_connected(),
        "host": Config.MQTT_HOST,
        "username": Config.MQTT_USERNAME
    })


######################
#
#   Download Stats
#

@app.route(API_URL + '/downloads/stats', methods=['GET'])
def api_download_stat_all():
    stats = download_stats.get_all_download_stats()
    return ApiResponse.json(stats)


@app.route(API_URL + '/downloads/stats/collect', methods=['POST'])
def api_download_stat_collect():
    stats = download_stats.collect_download_stats()
    return ApiResponse.json(stats)


@app.route(API_URL + '/downloads/stats/<int:id>', methods=['GET'])
def api_download_stat_get(id: int):
    stat = DownloadStat.query.get(id)
    return ApiResponse.json(stat)


@app.route(API_URL + "/downloads/stats/<int:id>", methods=["DELETE"])
def api_download_stat_delete(id: int):
    stat = DownloadStat.query.get(id)
    download_stats.delete_download_stat(stat)
    return ApiResponse.json()


@app.route(API_URL + "/downloads/stats/repositories", methods=["GET"])
def api_download_stat_names():
    stats = download_stats.collect_all_names()
    return ApiResponse.json(stats)


@app.route(API_URL + "/downloads/stats/repositories/<string:repository>/monthly", methods=["GET"])
def api_download_stats_all_from_last_month(repository: str):
    stats = download_stats.all_from_last_month(repository)
    return ApiResponse.json(stats)


@app.route(API_URL + "/downloads/stats/repositories/<string:repository>/asset/<string:name>/summary", methods=["GET"])
def api_download_stat_summary_name(repository: str, name: str):
    stats = download_stats.summary_for_name(repository, name)
    return ApiResponse.json(stats)


@app.route(API_URL + "/downloads/stats/repositories/<string:repository>/asset/<string:name>/monthly", methods=["GET"])
def api_download_stats_all_from_last_month_for_name(repository: str, name: str):
    stats = download_stats.all_from_last_month_for_asset(repository, name)
    return ApiResponse.json(stats)
