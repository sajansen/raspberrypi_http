import logging
import sys

from flask import Flask
from flask_cors import CORS
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from config import Config
from mqtt_connection import mqtt_connect, mqtt_disconnect

logger = logging.getLogger(__name__)
logging.basicConfig(
    format='[%(levelname)s] %(asctime)s %(name)s | %(message)s',
    level=Config.LOG_LEVEL)

app = Flask(__name__,
            static_url_path="",
            static_folder="frontend/build")
app.config.from_object(Config)
CORS(app)

db = SQLAlchemy(app)
migrate = Migrate(app, db)

from template_filters import *
from routes import *

from utils import MyJSONEncoder

app.json_encoder = MyJSONEncoder

if not Config.MQTT_ENABLE:
    logger.warning("MQTT is disabled in config")
else:
    try:
        mqtt_connect()
    except ConnectionRefusedError as e:
        logger.warning("Could not connect with MQTT broker")
        logger.exception(e, stack_info=True)
        sys.exit("Could not connect with MQTT broker")

if __name__ == '__main__':
    app.run(host=Config.HOST_IP)
    mqtt_disconnect()
