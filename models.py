import logging
import re
from datetime import time, datetime
from typing import Optional, List

from flask import jsonify

from app import db
from config import Config
from mqtt_connection import mqtt_client

logger = logging.getLogger(__name__)


class Serializable:
    def serialize(self):
        return ""


class CronJob(db.Model, Serializable):
    id: int = db.Column(db.Integer, primary_key=True)
    name: str = db.Column(db.String(256), nullable=False)
    script: str = db.Column(db.Text, nullable=False)
    execute_weekdays: str = db.Column(db.String(64), nullable=False)
    execute_time: Optional[time] = db.Column(db.Time, nullable=False)
    enabled: bool = db.Column(db.Boolean, default=True)

    allowed_weekdays = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
    execute_weekdays_separator = ";"

    def __repr__(self):
        return 'CronJob(id={}, name={})'.format(self.id, self.name)

    def serialize(self):
        return {
            "id": self.id,
            "name": self.name,
            "script": self.script,
            "execute_weekdays": self.execute_weekdays.split(self.execute_weekdays_separator),
            "execute_time": self.execute_time,
            "enabled": self.enabled,
        }

    def execute_weekdays_list(self) -> list:
        return self.execute_weekdays.split(self.execute_weekdays_separator)

    def from_json(self, json):
        logger.debug("Filling {} with values from JSON: {}".format(self, json))

        self.name = json["name"]
        self.script = json["script"]
        execute_weekdays = json["execute_weekdays"]
        execute_time = json["execute_time"]
        self.enabled = json["enabled"]

        if execute_weekdays is not None:
            self.execute_weekdays = self.execute_weekdays_separator.join(execute_weekdays)
        else:
            self.execute_weekdays = ""

        try:
            self.execute_time = datetime.strptime(execute_time, "%H:%M").time()
        except Exception:
            try:
                self.execute_time = datetime.strptime(execute_time, "%H:%M:%S").time()
            except Exception as e:
                logger.warning("'{}' has an invalid time format".format(execute_time))
                logger.exception(e, stack_info=True)
                self.execute_time = None

    def from_request(self, request):
        logger.debug("Filling {} with values from request".format(self))

        self.name = request.form.get("name", "")

        self.script = request.form.get("script", "")
        self.script = re.sub(r'(\n\r?)+', '\n', self.script)
        self.script = self.script.strip("\n")

        execute_weekdays = request.form.getlist("execute_weekdays[]", None)
        if execute_weekdays is not None:
            self.execute_weekdays = self.execute_weekdays_separator.join(execute_weekdays)
        else:
            self.execute_weekdays = ""

        execute_time: str = request.form.get("execute_time", "")
        try:
            self.execute_time = datetime.strptime(execute_time, "%H:%M").time()
        except Exception as e:
            logger.warning("'{}' has an invalid time format".format(execute_time))
            logger.warning(e)
            self.execute_time = None

        self.enabled = request.form.get("enabled", "") == "on"

    def validate(self) -> Optional[str]:
        logger.debug("Validating {}".format(self))

        if not self.name or self.name.strip() == "":
            return "Name cannot be empty"

        if not self.script or self.script.strip() == "":
            return "Script cannot be empty"

        if not self.execute_weekdays or self.execute_weekdays.strip() == "":
            return "Weekdays must be at least one"
        else:
            for weekday in self.execute_weekdays_list():
                if weekday not in self.allowed_weekdays:
                    return "{} is not an allowed weekday".format(weekday)

        if self.execute_time is None:
            return "Execute time cannot be empty"

        if self.enabled is None:
            return "Enabled must be a boolean"

        return None

    def execute(self):
        logger.info("Executing {}".format(self))
        for line in self.script.strip().split("\n"):
            logger.debug("Sending '{}' to MQTT topic '{}'".format(line, Config.MQTT_TOPIC))
            mqtt_client.publish(Config.MQTT_TOPIC, line)


class ApiResponse:
    RESPONSE_OK = "ok"
    RESPONSE_ERRORS = "error"

    @staticmethod
    def json(data: Optional = None,
             status: str = RESPONSE_OK,
             response_code: int = 200, ):
        return jsonify({
            "status": status,
            "response_code": response_code,
            "data": data,
        })

    # def __init__(self,
    #              data: Optional = None,
    #              status: str = RESPONSE_OK,
    #              response_code: int = 200,
    #              ):
    #     self.status = status
    #     self.response_code = response_code
    #     self.data = data
    #
    # def json(self) -> str:
    #     return jsonify({
    #         "status": self.status,
    #         "response_code": self.response_code,
    #         "data": self.data,
    #     })
    #
    # def __repr__(self):
    #     return self.json()


class DownloadStat(db.Model, Serializable):
    id: int = db.Column(db.Integer, primary_key=True)
    server: str = db.Column(db.String(255), nullable=False)
    user: str = db.Column(db.String(255), nullable=False)
    repository: str = db.Column(db.String(255), nullable=False)
    name: str = db.Column(db.String(255), nullable=False)
    download_count: int = db.Column(db.Integer, nullable=False, default=0)
    tag: str = db.Column(db.String(255), nullable=False, default="")
    created_at: datetime = db.Column(db.DateTime, nullable=False, default=datetime.now())

    def __init__(self, server: str,
                 user: str,
                 repository: str,
                 name: str,
                 download_count: int = 0,
                 tag: str = ""):
        self.server: str = server
        self.user: str = user
        self.repository: str = repository
        self.name: str = name
        self.download_count: int = download_count
        self.tag: str = tag
        self.created_at: datetime = datetime.now()

    def __repr__(self):
        return 'DownloadStat(id={}, name={})'.format(self.id, self.name)

    def serialize(self):
        return {
            "id": self.id,
            "server": self.server,
            "user": self.user,
            "repository": self.repository,
            "name": self.name,
            "download_count": self.download_count,
            "tag": self.tag,
            "created_at": self.created_at,
        }


class DownloadStatSummary(Serializable):
    def __init__(self,
                 repository: str,
                 name: str,
                 daily: int = 0,
                 weekly: int = 0,
                 monthly: int = 0,
                 ):
        self.repository: str = repository
        self.name: str = name
        self.daily: int = daily
        self.weekly: int = weekly
        self.monthly: int = monthly

    def serialize(self):
        return {
            "repository": self.repository,
            "name": self.name,
            "daily": self.daily,
            "weekly": self.weekly,
            "monthly": self.monthly,
        }


class TotalDownloadStatItem(Serializable):
    def __init__(self,
                 download_count: int,
                 created_at: datetime,
                 ):
        self.download_count: int = download_count
        self.created_at: datetime = created_at

    def serialize(self):
        return {
            "download_count": self.download_count,
            "created_at": self.created_at,
        }


class TotalDownloadStats(Serializable):
    def __init__(self,
                 downloads: List[TotalDownloadStatItem] = None,
                 ):
        self.downloads: List[TotalDownloadStatItem] = downloads if downloads is not None else []

    def serialize(self):
        return {
            "downloads": self.downloads,
        }

from modules.analog_sensors.models import ElectricityReading, ElectricityAbsoluteReading